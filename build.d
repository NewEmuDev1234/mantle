#!/usr/bin/rdmd
import std.zip,
       std.file,
       std.stdio,
       std.format,
       std.process;

void main() {
    //Create the default resource pack
    auto zip = new ZipArchive;
    mkdirRecurse("build/shader");

    chdir("asset/texture");
    foreach (i; dirEntries(".", SpanMode.shallow)) {
        auto member = new ArchiveMember;
        member.name = "texture/" ~ i.name[2..$];
        member.expandedData(cast(ubyte[])read(i.name));
        zip.addMember(member);
    }

    chdir("../sound");
    foreach (i; dirEntries(".", SpanMode.shallow)) {
        auto member = new ArchiveMember;
        member.name = "sound/" ~ i.name[2..$];
        member.expandedData(cast(ubyte[])read(i.name));
        zip.addMember(member);
    }

    chdir("../shader");
    foreach (i; dirEntries(".", SpanMode.shallow)) {
        if (i.name.length > 7 && i.name[$-5..$] == ".frag" || i.name[$-5..$] == ".vert") {
            string outputName = format("%s%s.o", i.name[$-4..$], i.name[2..$-5]);
            executeShell(format("glslc %s -o ../../build/shader/%s", i.name, outputName))[1].writeln;
            
            auto member = new ArchiveMember;
            member.name = "shader/" ~ outputName;
            member.expandedData(cast(ubyte[])read("../../build/shader/" ~ outputName));
            zip.addMember(member);
        }    
    }

    std.file.write("../../build/default.zip", zip.build);

    //Build the main program file
    chdir("../../");
    executeShell("ldc2 src/**.d src/render/**.d src/audio/**.d src/math/**.d -g --d-version=BindBC_Static --d-version=bindSDLImage --d-version=bindSDLMixer --d-version=SDL_209 --d-version=SDL_Mixer_204 --d-version=SDL_Image_205 --release --d-debug -od build -Jbuild")[1].writeln;
    std.file.rename("chunk", "build/mantle");
}
