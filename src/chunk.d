import std.math,
       std.parallelism;
import math.vector;
import render.model;

enum BlockType : ushort {
    Air,
    Dirt,
    Grass,
    Stone,
    Sand,
    Water,
    OakLog,
    OakLeaves
}

bool isOpaque(BlockType a) {
    switch (a) with (BlockType) {
        case Air:
        case Water:
        case OakLeaves:
            return false;
        default:
            return true;
    }
}

uint lightIntensity(BlockType a) {
    switch (a) with (BlockType) {
        case OakLeaves:
            return 10;
        default:
            return 0;
    }
}

vec3 lightColor(BlockType a) {
    switch (a) with (BlockType) {
        case OakLeaves:
            return vec3(0.5,1.0,0.5);
        default:
            assert(false, "Attempt to obtain color of non-glowing block type");
    }
}

struct Block {
    public:
        vec3 color = 0.05;
        float skyLight = 0;
        BlockType type;

        this(BlockType type) {
            this.type = type;
        }
}

class Chunk {
    private:
        bool _invalid;

    public:
        enum size = 32;
        Block[size][size][size] blocks;
        alias blocks this;

        Model[2] modelData;
        TexVertElement[] modelVert;
        uint[] modelIndex;
        TexVertElement[] transVert;
        uint[] transIndex;

        Task!(run, void delegate(Chunk, vec!(3LU, double), double), Chunk, vec!(3LU, double), double)* modelTask;
        bool taskSent;
        bool modelMade;

        bool postGen;

        bool invalid() {
            return _invalid;
        }

        void invalidate() { 
            taskSent  = false;
            modelMade = false;
            _invalid  = true;
        }

        void revalidate() {
            _invalid = false;
        }

        ~this() {
            modelData.destroy;
        }
}

ivec3 toChunkCoord()(auto ref dvec3 pos) {
    //Convert a block coordinate to a chunk coordinate
    ivec3 coord;
    foreach (i; 0..3) {
        coord[i] = cast(int)floor(pos[i]/Chunk.size);
    }
    return coord;
}

ivec3 toChunkBlock()(auto ref dvec3 pos) {
    //Convert a block coordinate to a chunk-relative block coordinate
    ivec3 block;
    foreach (i; 0..3) {
        if (pos[i] >= 0) {
            block[i] = cast(int)pos[i] % Chunk.size;
        } else if (pos[i] < 0) {
            block[i] = cast(int)(Chunk.size + floor(pos[i] % Chunk.size));
            if (block[i] == Chunk.size) {
                block[i] = 0;
            }
        }
    }
    return block;
}
