module render.model;

import math.shapes,
       math.vector,
       math.matrix;
import erupted;
public import render.buffers;
import render.pipeline;

struct Model {
    private:
        DeviceBuffer _buf;
        Pipeline* _pipeline;
        size_t _elementLen;
        size_t _indexLen;
        size_t _frameLen;
        bool _perFrame;

    public:
        ref DeviceBuffer buf()  { return _buf;        }
        ref Pipeline pipeline() { return *_pipeline;  }
        size_t elementLen()     { return _elementLen; }
        size_t indexLen()       { return _indexLen;   }
        bool perFrame()         { return _perFrame;   }
        size_t frameLen()       { return _frameLen;   }

        T[] data(T)(uint frame) {
            assert(_perFrame);
            auto base = _indexLen * uint.sizeof;
            return cast(T[])_buf.data!void[base + frame * _elementLen * _frameLen ..
                                           base + (frame+1) * _elementLen * _frameLen];
        }

        T[] data(T)() { 
            auto base = _indexLen * uint.sizeof;
            if (_perFrame) {
                return cast(T[])_buf.data!void[base + _pipeline.window.swapImage * _elementLen * _frameLen ..
                                               base + (_pipeline.window.swapImage+1) * _elementLen * _frameLen];
            } else {
                return cast(T[])_buf.data!void[base .. $];
            }
        }

        void pushStaging() {
            auto base = _indexLen * uint.sizeof;
            if (_perFrame) {
                buf.pushStaging(base + _pipeline.window.swapImage * _elementLen * _frameLen, _elementLen * _frameLen);
            } else {
                buf.pushStaging(base, buf.data!void.length - base);
            }
        }

        void loadNewModel(ref Pipeline pipeline, size_t elementLen, void[] vertices, uint[] index, bool perFrame = false) {
            if (vertices.length) {
                _buf = DeviceBuffer(pipeline.window.device, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, (index.length*uint.sizeof)+(vertices.length*(perFrame ? pipeline.window.frames.length : 1)));
                _buf.data!uint[0..index.length] = index[];
                auto base = index.length * uint.sizeof;

                /*Seperate vertex buffers should be maintained for each window swapchain frame when the model is likely to change a lot,
                 *as this should allow for modifying the models without modifying the data that an existing command buffer execution may be accessing.
                 *The index buffer is at the beginning of the memory allocation and shared between all vertex buffers at the end
                 */
                foreach (i; 0..perFrame ? pipeline.window.frames.length : 1) {
                    _buf.data!void[base + vertices.length*i .. base + vertices.length*(i+1)] = vertices[];
                }
                _buf.pushStaging;

                _pipeline = &pipeline;
                _elementLen = elementLen;
                _indexLen = index.length;
                _frameLen = vertices.length / elementLen;
                _perFrame = perFrame;
            }
        }

        this(ref Pipeline pipeline, size_t elementLen, void[] vertices, uint[] index, bool perFrame = false) {
            loadNewModel(pipeline, elementLen, vertices, index, perFrame);
        }
}

struct MultiModel(T = TexVertElement) {
    private:
        Model _model;
        T[][] _modelData;
        uint[][] _index;
        mat4[] _transforms;

    public:
        ref Model build(ref Pipeline pipeline) {
            //Build a vertex buffer containing the vertices of all submodels with the appropriate transformation matrices applied
            T[] buf;
            uint[] index;
            foreach (i, sub; _modelData) {
                //Apply transformations
                buf.reserve(sub.length);
                foreach (e, ref f; sub) {
                    buf ~= f;

                    auto vertex = vec4(f.xyz.x, f.xyz.y, f.xyz.z, 1);
                    vertex = _transforms[i] * vertex;
                    if (i != 0) {
                        vertex = _transforms[0] * vertex;
                    }

                    buf[$-1].xyz = vec3(vertex.x, vertex.y, vertex.z);
                }

                //Add indices to complete index buffer
                index ~= _index[i];
            }

            _model.loadNewModel(pipeline, T.sizeof, buf, index);
            return _model;
        }
        alias build this;

        mat4[] transforms() {
            return _transforms;
        }

        this(T[][] toAdd, uint[][] toAddIndex) {
            _modelData.reserve(toAdd.length);
            _transforms.reserve(toAdd.length);
            foreach (i; 0..toAdd.length) {
                _modelData ~= toAdd[i].dup;
                _transforms ~= identity;
            }
        }

        this(ref MultiModel b) {
            _modelData = b._modelData;
            _transforms = b._transforms.dup;
        }
}

T[8] getCube(T)() {
    T[8] vertices;
    foreach (i, ref e; vertices) {
        e.xyz = [vec3(0.0f,0.0f,0.0f), vec3(1.0f,0.0f,0.0f), vec3(0.0f,1.0f,0.0f), vec3(1.0f,1.0f,0.0f),
                 vec3(0.0f,0.0f,1.0f), vec3(1.0f,0.0f,1.0f), vec3(0.0f,1.0f,1.0f), vec3(1.0f,1.0f,1.0f)][i];
    }
    return vertices;
}

uint[36] cubeIndex = [0, 1, 2, 2, 1, 3,
                      5, 4, 7, 7, 4, 6,
                      4, 0, 6, 6, 0, 2,
                      1, 5, 3, 3, 5, 7,
                      2, 3, 6, 6, 3, 7,
                      4, 5, 0, 0, 5, 1];
