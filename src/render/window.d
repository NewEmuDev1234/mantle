module render.window;

import bindbc.sdl;
import erupted,
       erupted.vulkan_lib_loader;
import render.pass,
       render.image,
       render.device,
       render.buffers,
       render.instance;
import std.conv,
       std.file,
       std.stdio,
       std.format,
       std.exception;

struct Window {
    private:
        SDL_Window* _window;
        VkSurfaceKHR _surface;
        VkExtent2D _resolution;
        VkFormat _surfaceFormat;
        VkSwapchainKHR _swapchain;
        VkImageView[] _swapchainImage;
        VkFramebuffer[] _swapchainFrame;
        VkRenderPass[2] _pass;
        VkSampler _sampler;
        Image _depth;

        VkSemaphore[2][framesInFlight] _renderSemaphores;
        VkFence[framesInFlight] _renderFences;
        uint _curFrame;
        uint _swapImage;
        bool _invalid;
        Device* _device;

        void createWindow(string title, bool fullscreen) {
            //Create a window
            _window = SDL_CreateWindow((title ~ '\0').ptr, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, _resolution.width, _resolution.height, cast(SDL_WindowFlags)fullscreen*SDL_WINDOW_FULLSCREEN | SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
            enforce(_window, std.format.format("Failed to create _window: %s", SDL_GetError.to!string));
        }

        void createSurface() {
            //Create a window surface
            SDL_Vulkan_CreateSurface(_window, instance, &_surface);
            enforce(_surface, std.format.format("Failed to create _window _surface: %s", SDL_GetError.to!string));

            uint numFormat;
            vkGetPhysicalDeviceSurfaceFormatsKHR(_device.physical, _surface, &numFormat, null);
            VkSurfaceFormatKHR[] formats;
            formats.length = numFormat;
            vkGetPhysicalDeviceSurfaceFormatsKHR(_device.physical, _surface, &numFormat, formats.ptr);
            _surfaceFormat = formats[0].format;
        }

        void createRenderPass() {
            foreach (i; 0..2) {
                VkAttachmentDescription[2] attachmentDesc;
                with (attachmentDesc[0]) {
                    format        = _surfaceFormat;
                    samples       = VK_SAMPLE_COUNT_1_BIT;
                    loadOp        = VK_ATTACHMENT_LOAD_OP_LOAD;
                    storeOp       = VK_ATTACHMENT_STORE_OP_STORE;
                    initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                    finalLayout   = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
                }
                with (attachmentDesc[1]) {
                    format        = VK_FORMAT_D32_SFLOAT;
                    samples       = VK_SAMPLE_COUNT_1_BIT;
                    loadOp        = i ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
                    storeOp       = VK_ATTACHMENT_STORE_OP_DONT_CARE;
                    initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                    finalLayout   = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
                }

                VkAttachmentReference[2] attachmentRef;
                with (attachmentRef[0]) {
                    attachment = 0;
                    layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                }
                with (attachmentRef[1]) {
                    attachment = 1;
                    layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
                }
                
                VkSubpassDescription subpassDesc;
                with (subpassDesc) {
                    pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
                    colorAttachmentCount    = 1;
                    pColorAttachments       = &attachmentRef[0];
                    pDepthStencilAttachment = &attachmentRef[1];
                }

                VkSubpassDependency subpassDep;
                with (subpassDep) {
                    srcSubpass    = VK_SUBPASS_EXTERNAL;
                    dstSubpass    = 0;
                    srcStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                    dstStageMask  = srcStageMask;
                    dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                }

                VkRenderPassCreateInfo passInfo;
                with (passInfo) {
                    sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
                    attachmentCount = attachmentDesc.length;
                    pAttachments    = attachmentDesc.ptr;
                    subpassCount    = 1;
                    pSubpasses      = &subpassDesc;
                    dependencyCount = 1;
                    pDependencies   = &subpassDep;
                }

                vkCreateRenderPass(*_device, &passInfo, null, &_pass[i]);
            }
        }

        void createSampler() {
            VkSamplerCreateInfo samplerInfo;
            with (samplerInfo) {
                sType             = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
                magFilter         = VK_FILTER_NEAREST;
                minFilter         = VK_FILTER_NEAREST;
                addressModeU      = VK_SAMPLER_ADDRESS_MODE_REPEAT;
                addressModeV      = VK_SAMPLER_ADDRESS_MODE_REPEAT;
                addressModeW      = VK_SAMPLER_ADDRESS_MODE_REPEAT;
                anisotropyEnable  = VK_TRUE;
                maxAnisotropy     = _device.prop.limits.maxSamplerAnisotropy;
                mipmapMode        = VK_SAMPLER_MIPMAP_MODE_LINEAR;
                mipLodBias        = 0.0;
                minLod            = 0.0;
                maxLod            = 0.0;
            }
            vkCreateSampler(*_device, &samplerInfo, null, &_sampler);
        }

        void destroySwapchain() {
            foreach (i; _swapchainFrame) {
                vkDestroyFramebuffer(*_device, i, null);
            }
            _swapchainFrame.length = 0;
            foreach (i; _swapchainImage) {
                vkDestroyImageView(*_device, i, null);
            }
            _swapchainImage.length = 0;
            vkDestroySwapchainKHR(*_device, _swapchain, null);
        }

        void createSwapchain() {
            //Create a window surface and obtain information about it
            VkSurfaceCapabilitiesKHR capabilities;
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_device.physical, _surface, &capabilities);

            uint minImages = 2;
            if (capabilities.minImageCount > 2) {
                minImages = capabilities.minImageCount;
            } else if (capabilities.maxImageCount != 0 && capabilities.maxImageCount < 2) {
                minImages = capabilities.maxImageCount;
            }

            uint numFormat;
            vkGetPhysicalDeviceSurfaceFormatsKHR(_device.physical, _surface, &numFormat, null);
            VkSurfaceFormatKHR[] formats;
            formats.length = numFormat;
            vkGetPhysicalDeviceSurfaceFormatsKHR(_device.physical, _surface, &numFormat, formats.ptr);
            _surfaceFormat = formats[0].format;

            //Create a swapchain
            VkSwapchainCreateInfoKHR _swapchainInfo;
            with (_swapchainInfo) {
                sType                 = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
                surface               = _surface;
                minImageCount         = minImages;
                imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                imageFormat           = _surfaceFormat;
                imageColorSpace       = formats[0].colorSpace;
                imageExtent           = _resolution;
                imageArrayLayers      = 1;
                imageSharingMode      = _device.queueFamilies.length > 1 ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
                queueFamilyIndexCount = cast(uint)_device.queueFamilies.length;
                pQueueFamilyIndices   = _device.queueFamilies.ptr;  
                presentMode           = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
                clipped               = VK_TRUE;
            }
            vkCreateSwapchainKHR(*_device, &_swapchainInfo, null, &_swapchain);

            //Create image view and framebuffer objects wrapping each of the swapchain images
            uint numImages;
            vkGetSwapchainImagesKHR(*_device, _swapchain, &numImages, null);
            VkImage[] images;
            images.length = numImages;
            vkGetSwapchainImagesKHR(*_device, _swapchain, &numImages, images.ptr);

            foreach (i; images) {
                //Create an image view for each image in the _swapchain
                VkImageViewCreateInfo imageViewInfo;
                with (imageViewInfo) {
                    sType      = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                    image      = i;
                    format     = formats[0].format;
                    viewType   = VK_IMAGE_VIEW_TYPE_2D;
                    components = VkComponentMapping(VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY);
                    with (subresourceRange) {
                        aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
                        baseMipLevel   = 0;
                        levelCount     = 1;
                        baseArrayLayer = 0;
                        layerCount     = 1;
                    } 
                }
                _swapchainImage ~= null;
                vkCreateImageView(*_device, &imageViewInfo, null, &_swapchainImage[$-1]); 

                //Also create a framebuffer for the image view to use it as an attachment
                VkFramebufferCreateInfo framebufferInfo;
                with (framebufferInfo) {
                    sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                    renderPass      = _pass[0];
                    attachmentCount = 2;
                    pAttachments    = [_swapchainImage[$-1], _depth].ptr;
                    width           = _resolution.width;
                    height          = _resolution.height;
                    layers          = 1;
                }
                _swapchainFrame ~= null;
                vkCreateFramebuffer(*_device, &framebufferInfo, null, &_swapchainFrame[$-1]);
            }
        }

        void destroySyncPrimitives() {
            foreach (ref i; _renderSemaphores) {
                foreach (e; i) {
                    vkDestroySemaphore(*_device, e, null);
                }
            }
            foreach (i; _renderFences) {
                vkDestroyFence(*_device, i, null);
            }
        }

        void createSyncPrimitives() {
            foreach (ref i; _renderFences) {
                VkFenceCreateInfo fenceInfo;
                with (fenceInfo) {
                    sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                    flags = VK_FENCE_CREATE_SIGNALED_BIT;    
                }
                vkCreateFence(*_device, &fenceInfo, null, &i);
            }
            foreach (ref i; _renderSemaphores) {
                foreach (ref e; i) {
                    VkSemaphoreCreateInfo semaphoreInfo;
                    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
                    vkCreateSemaphore(*_device, &semaphoreInfo, null, &e);
                }
            }
        }

        void checkInvalid() {
            //Check if window surface has changed size or swapchain has previously been determined to be invalid
            int width;
            int height;
            SDL_GetWindowSize(_window, &width, &height);
            if (width != _resolution.width || height != _resolution.height || _invalid) {
                _resolution = VkExtent2D(width, height);
                _curFrame = 0;
                _invalid = true;
            }
        }

    public:
        VkExtent2D       resolution() { return _resolution;     }
        VkFramebuffer[]  frames()     { return _swapchainFrame; }
        VkRenderPass[2]  pass()       { return _pass;           }
        VkFormat         format()     { return _surfaceFormat;  }
        VkSampler        sampler()    { return _sampler;        }
        ref Device       device()     { return *_device;        }
        bool             invalid()    { return _invalid;        }
        uint             swapImage()  { return _swapImage;      }

        enum framesInFlight = 3;

        void recreate() {
            vkDeviceWaitIdle(*_device);

            _depth = Image(*_device, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, _resolution.width, _resolution.height);
            destroySwapchain;
            createSwapchain;
            destroySyncPrimitives;
            createSyncPrimitives;
            _curFrame = 0;
            _invalid = false;
        }

        void reset() {
            checkInvalid;
            if (_invalid) {
                return;
            }

            //Wait for a previous frame in flight to finish it's rendering operations, and then acquire an image
            vkWaitForFences(*_device, 1, [_renderFences[_curFrame]].ptr, VK_FALSE, ulong.max);
            vkResetFences(*_device, 1, [_renderFences[_curFrame]].ptr);
            if (vkAcquireNextImageKHR(*_device, _swapchain, ulong.max, _renderSemaphores[_curFrame][0], null, &_swapImage) == VK_ERROR_OUT_OF_DATE_KHR) {
                _invalid = true;
            }
        }

        void present(RenderPass*[] toRender ...) {
            checkInvalid;
            if (_invalid) {
                return;
            }

            //Submit the command buffers to execute for each render pass passed
            VkSubmitInfo submission;
            with (submission) {
                sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                waitSemaphoreCount   = 1;
                pWaitSemaphores      = &_renderSemaphores[_curFrame][0];
                signalSemaphoreCount = 1;
                pWaitDstStageMask    = cast(uint*)[VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT].ptr;
                pSignalSemaphores    = &_renderSemaphores[_curFrame][1];
                commandBufferCount   = cast(uint)toRender.length;

                VkCommandBuffer[] cmdBuf;
                cmdBuf.reserve(toRender.length);
                foreach (i; toRender) {
                    cmdBuf ~= i.modelCmd[_swapImage];
                }
                pCommandBuffers = cmdBuf.ptr;
            }
            vkQueueSubmit(_device.queueHandles[0], 1, &submission, _renderFences[_curFrame]);

            //Present the rendered image and update the next frame in flight to use
            VkPresentInfoKHR presentation;
            with (presentation) {
                sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
                waitSemaphoreCount = 1;
                pWaitSemaphores    = &_renderSemaphores[_curFrame][1];
                swapchainCount     = 1;
                pSwapchains        = &_swapchain;
                pImageIndices      = &_swapImage;
            }
            vkQueuePresentKHR(_device.queueHandles[$-1], &presentation);

            _curFrame = (_curFrame+1) % framesInFlight;
        }

        this(ref Device device, string title, int width, int height, bool fullscreen) {
            _device = &device;
            if (width <= 0 || height <= 0) {
                SDL_Rect resolution;
                SDL_GetDisplayBounds(0, &resolution);
                _resolution = VkExtent2D(resolution.w, resolution.h);
            } else {
                _resolution = VkExtent2D(width, height);
            }

            //Create a window and initialise the device object
            createWindow(title, fullscreen);
            initInstance(_window);
            device.findPhysicalDevice;
            createSurface;

            //Create a logical device
            device.findQueueFamiliesPresent(_surface, VK_QUEUE_GRAPHICS_BIT);
            device.initDevice;

            //Create the rest of the objects needed for a window
            createRenderPass;
            _depth = Image(device, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, _resolution.width, _resolution.height);
            createSwapchain;
            createSyncPrimitives;
            createSampler;
        }

        @disable this();

        ~this() {
            vkDeviceWaitIdle(*_device);

            //Destroy the render pass and sampler
            foreach (i; _pass) {
                vkDestroyRenderPass(*_device, i, null);
            }            
            vkDestroySampler(*_device, _sampler, null);

            //Destroy synchronisation primitives
            destroySyncPrimitives;

            //Destroy swapchain, window surface and window
            destroySwapchain;
            vkDestroySurfaceKHR(instance, _surface, null);
            SDL_DestroyWindow(_window);
        }
}
