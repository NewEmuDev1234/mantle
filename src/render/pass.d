module render.pass;

import std.exception;
import erupted;
import render.model,
       render.window,
       render.buffers;

struct RenderPass {
    private:
        Window* _window;
        bool _clear;
        Model*[] _models;
        VkCommandBuffer[] _cmdBuf;

        void createCmdBuf(size_t frame) {
            _cmdBuf[frame] = _window.device.createCommandBuf((VkCommandBuffer cmdBuf){
                //For each command buffer: create a render pass instance, bind the pipeline and vertex buffer, and then create a draw command for the vertex buffer
                VkRenderPassBeginInfo renderPassInfo;
                with (renderPassInfo) {
                    sType           = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                    renderPass      = _window.pass[_clear];
                    framebuffer     = _window.frames[frame];
                    renderArea      = VkRect2D(VkOffset2D(0,0), _window.resolution);

                    VkClearValue[2] clear;
                    clear[1].depthStencil = VkClearDepthStencilValue(1.0, 0);
                    clearValueCount = 2;
                    pClearValues = clear.ptr;
                }
                vkCmdBeginRenderPass(cmdBuf, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

                foreach (e; _models) {
                    if (e.elementLen > 0) {
                        vkCmdBindPipeline(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, e.pipeline);
                        vkCmdBindVertexBuffers(cmdBuf, 0, 1, [e.buf.handle].ptr, [e.indexLen*uint.sizeof].ptr);
                        vkCmdBindIndexBuffer(cmdBuf, e.buf.handle, 0, VK_INDEX_TYPE_UINT32);
                        vkCmdBindDescriptorSets(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, e.pipeline.layout, 0, 1 + (e.pipeline.sets.length > _window.frames.length), [e.pipeline.sets[frame], e.pipeline.sets[$-1]].ptr, 0, null);
                        vkCmdDrawIndexed(cmdBuf, cast(uint)e.indexLen, 1, 0, e.perFrame ? cast(int)(e.frameLen*frame) : 0, 0);
                    }
                }

                vkCmdEndRenderPass(cmdBuf);
            });
            enforce(_cmdBuf[frame], "Failed to create command buffer for model");
        }

        void createCmdBuf() {
            //Create command buffers for rendering the model using the given pipeline and each swapchain frame
            _cmdBuf.length = _window.frames.length;
            foreach (i; 0.._window.frames.length) {
                createCmdBuf(i);
            }
        }

    public:
        Model*[] models()            { return _models; }
        VkCommandBuffer[] modelCmd() { return _cmdBuf; }
        alias models this;

        void recreate() {
            vkDeviceWaitIdle(_window.device);
            _window.device.freeCommandBuf(_cmdBuf);
            createCmdBuf;
        }

        void recreateFrame() {
            _cmdBuf.length = _window.frames.length;
            if (_cmdBuf[_window.swapImage]) {
                _window.device.freeCommandBuf(_cmdBuf[_window.swapImage]);
            }
            createCmdBuf(_window.swapImage);
        }

        void reset() {
            _models = null;
        }

        void add(ref Model model) {
            _models ~= &model;
        }

        this(ref Window window, bool clear, Model*[] models ...) {
            this(window, clear);
            _models = models.dup;
            createCmdBuf;
        }

        this(ref Window window, bool clear) {
            _window = &window;
            _clear  = clear;
        }

        @disable this();

        ~this() {
            if (_window) {
                vkDeviceWaitIdle(_window.device);
                _window.device.freeCommandBuf(_cmdBuf);
            }
        }
}
