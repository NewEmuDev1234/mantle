module render.device;

import std.conv,
       std.format,
       std.exception;
import erupted,
       erupted.vulkan_lib_loader;
import render.window,
       render.instance;

struct Device {
    private:
        VkPhysicalDevice _physical;
        VkDevice _device;
        uint[] _queueFamilies;
        VkQueue[] _queues;
        VkCommandPool _commandPool;
        VkPhysicalDeviceProperties _properties;

        static uint _deviceCount;
        static string[] _extensions = ["VK_KHR_swapchain"];

        void createDevice() {
            //Create a logical device
            VkDeviceQueueCreateInfo[] queueInfo;
            queueInfo.length = _queueFamilies.length;
            foreach (i, ref e; queueInfo) with (e) {
                queueFamilyIndex = _queueFamilies[i];
                queueCount       = 1;
                pQueuePriorities = [1.0f].ptr;
            }

            //Select features to use
            VkPhysicalDeviceFeatures featureSelect;
            with (featureSelect) {
                samplerAnisotropy = VK_TRUE;
            }            

            //Create the logical device
            VkDeviceCreateInfo deviceInfo;
            with (deviceInfo) {
                sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
                enabledExtensionCount   = cast(uint)_extensions.length;
                ppEnabledExtensionNames = strToCSlice(_extensions);
                pEnabledFeatures        = &featureSelect;
                queueCreateInfoCount    = cast(uint)queueInfo.length;
                pQueueCreateInfos       = queueInfo.ptr;
            }
            vkCreateDevice(_physical, &deviceInfo, null, &_device);

            //Obtain handles to the created queues (should be in order right?)
            _queues.length = queueInfo.length;
            foreach (i, ref e; queueInfo) {
                vkGetDeviceQueue(_device, e.queueFamilyIndex, 0, &_queues[i]);
            }
        }

        void createCommandPool() {
            VkCommandPoolCreateInfo poolInfo;
            with (poolInfo) {
                sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                queueFamilyIndex = _queueFamilies[0];
            }
            vkCreateCommandPool(_device, &poolInfo, null, &_commandPool);
        }

    public:
        VkDevice         handle()        { return _device;        }
        VkPhysicalDevice physical()      { return _physical;      }
        uint[]           queueFamilies() { return _queueFamilies; }
        VkQueue[]        queueHandles()  { return _queues;        }
        VkPhysicalDeviceProperties prop() { return _properties;   }
        alias handle this;

        void findQueueFamilies(VkQueueFlags[] type ...) {
            uint numFamilies;
            vkGetPhysicalDeviceQueueFamilyProperties(_physical, &numFamilies, null);
            VkQueueFamilyProperties[] _properties;
            _properties.length = numFamilies;
            vkGetPhysicalDeviceQueueFamilyProperties(_physical, &numFamilies, _properties.ptr);

            _queueFamilies = [];
            foreach (i; type) {
                bool found;
                uint index;
                foreach (e, ref f; _properties) {
                    if (f.queueFlags & i) {
                        found = true;
                        index = cast(uint)e; 
                        break;
                    }
                }
                enforce(found, format("Failed to find queue family with %s command support", i));
                _queueFamilies ~= index;
            }
        }

        void findQueueFamiliesPresent(VkSurfaceKHR surface, VkQueueFlags[] type ...) {
            findQueueFamilies(type);
            uint numFamilies;
            vkGetPhysicalDeviceQueueFamilyProperties(_physical, &numFamilies, null);

            foreach (i; 0..numFamilies) {
                VkBool32 present;
                vkGetPhysicalDeviceSurfaceSupportKHR(_physical, i, surface, &present);
                if (present) {
                    foreach (e; _queueFamilies) {
                        if (e == i) {
                            return;
                        }
                    }
                    _queueFamilies ~= i;
                    return;
                }
            }
            throw new Exception("Failed to find presentation queue family");
        }

        uint[] findIdealMemTypes(VkMemoryPropertyFlags properties) {
            VkPhysicalDeviceMemoryProperties memProp;
            vkGetPhysicalDeviceMemoryProperties(_physical, &memProp);

            uint[] idealMemTypes;
            foreach (i, ref e; memProp.memoryTypes[0..memProp.memoryTypeCount]) {
                if ((e.propertyFlags & properties) == properties) {
                    idealMemTypes ~= cast(uint)i;
                }
            }
            return idealMemTypes;
        }

        void findPhysicalDevice() {
            //Find a physical device to use
            uint numDevices;
            vkEnumeratePhysicalDevices(instance, &numDevices, null);
            VkPhysicalDevice[] physicals;
            physicals.length = numDevices;
            vkEnumeratePhysicalDevices(instance, &numDevices, physicals.ptr);
            _physical = physicals[0];
            vkGetPhysicalDeviceProperties(_physical, &_properties);
            
            foreach_reverse (i; physicals) {
                VkPhysicalDeviceProperties properties;
                vkGetPhysicalDeviceProperties(i, &properties);
                if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
                    _physical = i;
                    _properties = properties;
                }
            }
        }

        VkCommandBuffer createCommandBuf(void delegate(VkCommandBuffer) recorder, bool singleUse = false) {
            //Create and command buffers for each framebuffer, rendering the vertex data using the specified pipeline
            VkCommandBufferAllocateInfo cmdAllocInfo;
            with (cmdAllocInfo) {
                sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                commandBufferCount = 1;
                commandPool        = _commandPool;            
            }

            //Allocate a buffer and begin it's recording
            VkCommandBuffer cmdBuf;
            vkAllocateCommandBuffers(_device, &cmdAllocInfo, &cmdBuf);
            enforce(cmdBuf, "Failed to create command buffer");

            VkCommandBufferBeginInfo beginInfo;
            with (beginInfo) {
                sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
                flags = singleUse*VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            }
            vkBeginCommandBuffer(cmdBuf, &beginInfo);
            recorder(cmdBuf);
            vkEndCommandBuffer(cmdBuf);
            return cmdBuf;
        }

        void executeCommandBuf(void delegate(VkCommandBuffer) recorder) {
            auto cmdBuf = createCommandBuf(recorder, true);

            VkSubmitInfo submit;
            with (submit) {
                sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                commandBufferCount = 1;
                pCommandBuffers    = &cmdBuf;
            }
            vkQueueSubmit(queueHandles[0], 1, &submit, null);
            vkQueueWaitIdle(queueHandles[0]);
            freeCommandBuf(cmdBuf);
        }

        void freeCommandBuf(VkCommandBuffer[] buf ...) {
            if (buf.length) {
                vkFreeCommandBuffers(_device, _commandPool, cast(uint)buf.length, buf.ptr);
            }        
        }

        void initDevice() {
            //Create a logical device and render pass
            createDevice;
            createCommandPool;
        }

        ~this() {
            if (_device) {
                vkDeviceWaitIdle(_device);
                vkDestroyCommandPool(_device, _commandPool, null);
                if (_device) {
                    vkDestroyDevice(_device, null);
                }
                _deviceCount -= 1;
            }
        }
}
