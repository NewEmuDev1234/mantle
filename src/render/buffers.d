module render.buffers;

import std.exception;
import erupted;
import math.vector,
       math.matrix;
import render.device,
       render.pipeline;

struct Buffer {
    private:
        ubyte[] _map;
        size_t _len;
        VkBuffer _buf;
        VkDeviceMemory _mem;
        Device* _device;

        void createBuf(size_t len, VkBufferUsageFlags usageFlags, VkMemoryPropertyFlags typeFlags) {
            //Create a buffer object
            VkBufferCreateInfo bufferInfo;
            with (bufferInfo) {
                sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
                size        = len;
                usage       = usageFlags;
                sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            }
            vkCreateBuffer(_device.handle, &bufferInfo, null, &_buf);
            enforce(_buf, "Failed to create buffer object");

            //Create and bind a memory allocation for that buffer object
            VkMemoryRequirements requirements;
            vkGetBufferMemoryRequirements(_device.handle, _buf, &requirements);

            VkMemoryAllocateInfo allocateInfo;
            with (allocateInfo) {
                sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
                allocationSize  = requirements.size;
                memoryTypeIndex = _device.findIdealMemTypes(typeFlags)[0];
            }
            vkAllocateMemory(_device.handle, &allocateInfo, null, &_mem);
            enforce(_mem, "Failed to allocate memory");
            vkBindBufferMemory(_device.handle, _buf, _mem, 0);

            _len = len;
        }

    public:
        VkBuffer handle()          { return _buf; }
        VkDeviceMemory memHandle() { return _mem; }
        alias handle this;

        T[] data(T)() {
            if (!_map) {
                ubyte* mapData;
                enforce(vkMapMemory(_device.handle, _mem, 0, _len, 0, cast(void**)&mapData) != VK_ERROR_MEMORY_MAP_FAILED, "Failed to access staging buffer");
                _map = mapData[0.._len];
            }
            return cast(T[])_map;
        }

        void loadNewBuf(ref Device device, VkBufferUsageFlags usage, VkMemoryPropertyFlags memFlags, size_t len) {
            this.destroy;
            assert(len, "Attempt to create a Vulkan buffer of 0 length");
            _device = &device;
            createBuf(len, usage, memFlags);
        }

        void loadNewBuf(ref Device device, VkBufferUsageFlags usage, VkMemoryPropertyFlags memFlags, void[] data) {
            loadNewBuf(device, usage, memFlags, data.length);
            this.data!void[] = data[];
        }

        this(ref Device device, VkBufferUsageFlags usage, VkMemoryPropertyFlags memFlags, size_t len) { 
            loadNewBuf(device, usage, memFlags, len);      
        }

        this (ref Device device, VkBufferUsageFlags usage, VkMemoryPropertyFlags memFlags, void[] data) {
            loadNewBuf(device, usage, memFlags, data);
        }

        ~this() {
            if (_device) {
                vkDeviceWaitIdle(_device.handle);

                //Unmap the staging buffer allocation and then destroy all objects and allocations
                if (_map) {
                    vkUnmapMemory(_device.handle, _mem);
                }
                vkDestroyBuffer(_device.handle, _buf, null);
                vkFreeMemory(_device.handle, _mem, null);
            }
        }
}

struct DeviceBuffer {
    private:
        Buffer _staging;
        Buffer _actual;
        Device* _device;

    public:
        T[] data(T)()     { return _staging.data!T;  }
        VkBuffer handle() { return _actual;          }
        alias handle this;

        void removeStaging() {
            _staging.destroy;
        }

        void pushStaging(size_t offset, size_t len) {
            //Transfer the data from the staging buffer to the device local buffer
            VkBufferCopy region;
            with (region) {
                srcOffset = offset;
                dstOffset = offset;
                size      = len;
            }
            _device.executeCommandBuf((VkCommandBuffer cmdBuf) => vkCmdCopyBuffer(cmdBuf, _staging, _actual, 1, &region));
        }

        void pushStaging() {
            pushStaging(0, data!void.length);
        }

        void loadNewBuf(ref Device device, VkBufferUsageFlags usage, size_t len) {
            this.destroy;
            _device = &device;
            _staging.loadNewBuf(device, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, len);
            _actual.loadNewBuf(device, usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, len);
        }

        void loadNewBuf(ref Device device, VkBufferUsageFlags usage, void[] data) {
            loadNewBuf(device, usage, data.length);
            this.data!void[] = data[];
        }

        this(ref Device device, VkBufferUsageFlags usage, size_t len) {
            loadNewBuf(device, usage, len);
        }

        this(ref Device device, VkBufferUsageFlags usage, void[] data) {
            loadNewBuf(device, usage, data);
        }
}

struct TexVertElement {
    enum usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

    align(16) vec3 xyz;
    align(4)  uint textureIndex;
    align(16) vec3 textureCoord;
    align(4)  float skyLight;
    align(16) vec3 color;

    static VkVertexInputAttributeDescription[5] description() {
        VkVertexInputAttributeDescription[5] toReturn;
        with (toReturn[0]) {
            location = 0;
            format   = VK_FORMAT_R32G32B32_SFLOAT;
            offset   = xyz.offsetof;
        }
        with (toReturn[1]) {
            location = 1;
            format   = VK_FORMAT_R32_UINT;
            offset   = textureIndex.offsetof;
        }
        with (toReturn[2]) {
            location = 2;
            format   = VK_FORMAT_R32G32B32_SFLOAT;
            offset   = textureCoord.offsetof;
        }
        with (toReturn[3]) {
            location = 3;
            format   = VK_FORMAT_R32_SFLOAT;
            offset   = skyLight.offsetof;
        }
        with (toReturn[4]) {
            location = 4;
            format   = VK_FORMAT_R32G32B32_SFLOAT;
            offset   = color.offsetof;
        }
        return toReturn;
    }
}

struct ColVertElement {
    enum usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

    align(16) vec3 xyz;
    align(16) vec4 rgb;

    static VkVertexInputAttributeDescription[2] description() {
        VkVertexInputAttributeDescription[2] toReturn;
        with (toReturn[0]) {
            location = 0;
            format   = VK_FORMAT_R32G32B32_SFLOAT;
            offset   = xyz.offsetof;
        }
        with (toReturn[1]) {
            location = 1;
            format   = VK_FORMAT_R32G32B32_SFLOAT;
            offset   = rgb.offsetof;
        }
        return toReturn;
    }
}

struct UBO {
    align(16) mat4 transform;
}
