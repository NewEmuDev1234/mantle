module render.pipeline;

import erupted;
import render.image,
       render.device,
       render.window,
       render.buffers;
import std.zip,
       std.file,
       std.path,
       std.stdio,
       std.exception;

struct Pipeline {
    private:
        VkPipeline _pipeline;
        VkPipelineLayout _layout;
        Buffer[] _ubos;
        VkDescriptorSetLayout[2] _descriptorLayout;
        VkDescriptorPool _pool;
        VkDescriptorSet[] _set;
        size_t _size;
        VkVertexInputAttributeDescription[] _attribDesc;
        size_t _uboSize;
        bool _depthTest;

        VkShaderModule[] _shaders;
        VkShaderStageFlagBits[] _shaderStages;
        ubyte[][] _shaderCode;

        Window* _window;
        Texture[] _textures;

        void loadShaders(ZipArchive archive, string[] shaderNames) {
            _shaders.length       = shaderNames.length;
            _shaderStages.length  = shaderNames.length;
            _shaderCode.length    = shaderNames.length;

            //Create shader module objects for each shader path given
            foreach (i,e; shaderNames) {
                VkShaderModuleCreateInfo moduleInfo;
                with (moduleInfo) {
                    _shaderCode[i] = archive.expand(archive.directory[e]);

                    sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
                    pCode    = cast(uint*)(_shaderCode[i].ptr);
                    codeSize = _shaderCode[i].length;
                }
                
                vkCreateShaderModule(_window.device, &moduleInfo, null, &_shaders[i]);
            }

            //Determine the type of each shader
            foreach (i,e; shaderNames) {
                final switch (baseName(e)[0..4]) {
                    case "vert": _shaderStages[i] = VK_SHADER_STAGE_VERTEX_BIT  ; break;
                    case "frag": _shaderStages[i] = VK_SHADER_STAGE_FRAGMENT_BIT; break;
                }
            }
        }

        void createPipeline(Pipeline* toDerive = null) {
            //Configure shaders to use
            VkPipelineShaderStageCreateInfo[] stageInfo;
            stageInfo.length = _shaders.length;
            foreach (i, ref e; stageInfo) with (e) {
                stage  = _shaderStages[i];
                Module = _shaders[i];
                pName  = "main\0".ptr;
            }

            //Configure vertex buffer usage
            VkVertexInputBindingDescription bindingDesc;
            with (bindingDesc) {
                binding   = 0;
                stride    = cast(uint)_size;
                inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            }

            VkPipelineVertexInputStateCreateInfo vertexBufInfo;
            with (vertexBufInfo) {
                vertexBindingDescriptionCount   = 1;
                pVertexBindingDescriptions      = &bindingDesc;
                vertexAttributeDescriptionCount = cast(uint)_attribDesc.length;
                pVertexAttributeDescriptions    = _attribDesc.ptr;
            }

            //Configure input assembly
            VkPipelineInputAssemblyStateCreateInfo inputAssembly;
            with (inputAssembly) {
                topology               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
                primitiveRestartEnable = VK_FALSE;
            }

            //Configure viewport and scissors
            auto viewport = VkViewport(0, 0, _window.resolution.width, _window.resolution.height, 0.0, 1.0);
            auto scissor  = VkRect2D(VkOffset2D(0,0), _window.resolution);            

            VkPipelineViewportStateCreateInfo viewportInfo;
            with (viewportInfo) {
                viewportCount = 1;
                pViewports    = &viewport;
                scissorCount  = 1;
                pScissors     = &scissor;
            }

            //Configure rasterizer
            VkPipelineRasterizationStateCreateInfo rasterizerState;
            with (rasterizerState) {
                polygonMode    = VK_POLYGON_MODE_FILL;
                lineWidth      = 1.0;
                cullMode       = VK_CULL_MODE_BACK_BIT;
                frontFace      = VK_FRONT_FACE_COUNTER_CLOCKWISE;
                depthBiasClamp = 0.0;
            }

            //Configure multisampling
            VkPipelineMultisampleStateCreateInfo multisampleInfo;
            with (multisampleInfo) {
                rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
            }

            //Configure depth testing
            VkPipelineDepthStencilStateCreateInfo depthInfo;
            with (depthInfo) {
                depthTestEnable  = true;
                depthWriteEnable = _depthTest;
                depthCompareOp   = VK_COMPARE_OP_LESS_OR_EQUAL;
            }

            //Configure color blending
            VkPipelineColorBlendAttachmentState frameBlend;
            with (frameBlend) {
                colorWriteMask      = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
                blendEnable         = VK_TRUE;
                srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
                dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
                colorBlendOp        = VK_BLEND_OP_ADD;
            }
            VkPipelineColorBlendStateCreateInfo blendInfo;
            with (blendInfo) {
                attachmentCount = 1;
                pAttachments    = &frameBlend;
            }

            //Create descriptor layouts for a pipeline layout
            VkDescriptorSetLayoutBinding[2] descriptorBinding;
            foreach (i, ref e; descriptorBinding) with (e) {
                binding         = cast(uint)i;
                descriptorType  = i ? VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER : VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                descriptorCount = i ? cast(uint)_textures.length : 1;
                stageFlags      = i ? VK_SHADER_STAGE_FRAGMENT_BIT : VK_SHADER_STAGE_VERTEX_BIT;                
            }
            VkDescriptorSetLayoutCreateInfo[2] layoutInfo;
            foreach (i, ref e; layoutInfo) with (e) {
                sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
                bindingCount = 1;
                pBindings    = &descriptorBinding[i];
                vkCreateDescriptorSetLayout(_window.device, &e, null, &_descriptorLayout[i]);
            }

            VkPipelineLayoutCreateInfo pipelineLayoutInfo;
            with (pipelineLayoutInfo) {
                sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
                setLayoutCount = _textures.length ? 2 : 1;
                pSetLayouts    = _descriptorLayout.ptr;
            }
            vkCreatePipelineLayout(_window.device, &pipelineLayoutInfo, null, &_layout);

            //Create the graphics pipeline
            VkGraphicsPipelineCreateInfo _pipelineInfo;
            with (_pipelineInfo) {
                sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
                flags               = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
                layout              = _layout;
                stageCount          = cast(uint)_shaders.length;
                pStages             = stageInfo.ptr;
                pVertexInputState   = &vertexBufInfo;
                pInputAssemblyState = &inputAssembly;
                pViewportState      = &viewportInfo;
                pRasterizationState = &rasterizerState;
                pMultisampleState   = &multisampleInfo;
                pDepthStencilState  = &depthInfo;
                pColorBlendState    = &blendInfo;                
                renderPass          = _window.pass[0];
                subpass             = 0;
                basePipelineHandle  = toDerive != null ? *toDerive : null;
            }
            vkCreateGraphicsPipelines(_window.device, null, 1, &_pipelineInfo, null, &_pipeline);
            enforce(_pipeline, "Failed to create graphics pipeline");

            //Create a descriptor pool and set for the UBO to be used
            _set.length = _window.frames.length + (_textures.length > 0);

            VkDescriptorPoolSize[2] _poolSize;
            with (_poolSize[0]) {
                type            = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                descriptorCount = cast(uint)_window.frames.length;
            }
            with (_poolSize[1]) {
                type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptorCount = cast(uint)_textures.length;
            }

            VkDescriptorPoolCreateInfo _poolInfo;
            with (_poolInfo) {
                sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
                poolSizeCount = _textures.length ? 2 : 1;
                pPoolSizes    = _poolSize.ptr;
                maxSets       = cast(uint)(_window.frames.length + (_textures.length > 0));
            }
            vkCreateDescriptorPool(_window.device, &_poolInfo, null, &_pool);

            //Allocate a descriptor set for each UBO to be used
            VkDescriptorSetAllocateInfo setInfo;
            VkDescriptorSetLayout[] layouts;
            layouts.length = _window.frames.length + (_textures.length > 0);
            layouts[] = _descriptorLayout[0];
            if (_textures.length > 0) {
                layouts[$-1] = _descriptorLayout[1];
            }
            
            with (setInfo) {
                sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
                descriptorPool     = _pool;
                descriptorSetCount = cast(uint)layouts.length;
                pSetLayouts        = layouts.ptr;
            }
            vkAllocateDescriptorSets(_window.device, &setInfo, _set.ptr);

            _ubos.length = _window.frames.length;
            foreach (i; 0.._window.frames.length) {
                _ubos[i].loadNewBuf(_window.device, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, _uboSize);

                //Configure a descriptor set for each UBO to be used
                VkDescriptorBufferInfo uboInfo;
                with (uboInfo) {
                    buffer = _ubos[i];
                    offset = 0;
                    range  = _uboSize;
                }

                VkWriteDescriptorSet setConfigInfo;
                with (setConfigInfo) {
                    sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                    dstSet          = _set[i];
                    dstBinding      = 0;
                    descriptorCount = 1;
                    descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    pBufferInfo     = &uboInfo;
                }
                vkUpdateDescriptorSets(_window.device, 1, &setConfigInfo, 0, null);
            }

            //Configure the texture descriptor _sets
            foreach (i; 0.._textures.length) {
                VkDescriptorImageInfo imageInfo;
                with (imageInfo) {
                    imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                    imageView   = _textures[i];
                    sampler     = _window.sampler;
                }

                VkWriteDescriptorSet setConfigInfo;
                with (setConfigInfo) {
                    sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                    dstSet          = _set[$-1];
                    dstBinding      = 1;
                    dstArrayElement = cast(uint)i;
                    descriptorCount = 1;
                    descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                    pImageInfo      = &imageInfo;
                }
                vkUpdateDescriptorSets(_window.device, 1, &setConfigInfo, 0, null);
            }
        }

        void destroyPipeline() {
            vkDestroyPipeline(_window.device, _pipeline, null);
            vkDestroyPipelineLayout(_window.device, _layout, null);
            foreach (i; _descriptorLayout) {
                vkDestroyDescriptorSetLayout(_window.device, i, null);
            }
            vkDestroyDescriptorPool(_window.device, _pool, null);
        }

    public:
        ref T             ubo(T)() { return _ubos[_window.swapImage].data!T[0];   }
        ref Window        window() { return *_window;                             }
        VkPipeline        handle() { return _pipeline;                            }
        VkDescriptorSet[] sets()   { return _set;                                 }
        VkPipelineLayout  layout() { return _layout;                              }
        alias handle this;

        void recreate() {
            vkDeviceWaitIdle(_window.device);
            destroyPipeline;
            createPipeline;
        }

        Pipeline derive(ZipArchive archive, string[] shaders) {
            return Pipeline(this, *_window, _size, _attribDesc, _uboSize, _depthTest, archive, shaders, _textures);
        }

        this(ref Window window, size_t size, VkVertexInputAttributeDescription[] attribDesc, size_t uboSize, bool depthTest, ZipArchive archive, string[] shaders, Texture[] textures = null) {
            _window     = &window;
            _size       = size;
            _attribDesc = attribDesc;
            _uboSize    = uboSize;
            _depthTest  = depthTest;
            _textures   = textures;
            loadShaders(archive, shaders);
            createPipeline;
        }

        this(ref Pipeline pipeline, ref Window window, size_t size, VkVertexInputAttributeDescription[] attribDesc, size_t uboSize, bool depthTest, ZipArchive archive, string[] shaders, Texture[] textures = null) {
            _window     = &window;
            _size       = size;
            _attribDesc = attribDesc;
            _depthTest  = depthTest;
            _textures   = textures;
            loadShaders(archive, shaders);
            createPipeline(&pipeline);
        }

        @disable this();

        ~this() {
            foreach (i; _shaders) {
                vkDestroyShaderModule(_window.device, i, null);
            }
            destroyPipeline;

            //Destroy uniform buffers
            foreach (ref i; _ubos) {
                i.destroy;
            }
            _ubos.length = 0;
        }
}

Pipeline pipeline(vbo, ubo = UBO)(ref Window window, bool depthTest, ZipArchive archive, string[] shaders, Texture[] textures = null) {
    return Pipeline(window, vbo.sizeof, vbo.description, ubo.sizeof, depthTest, archive, shaders, textures);
}
