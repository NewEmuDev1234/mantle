module render.image;

import bindbc.sdl;
import std.zip,
       std.file,
       std.conv,
       std.format,
       std.exception;
import util;
import erupted;
import render.device,
       render.buffers;

struct ImageElement {
    enum usage = VkBufferUsageFlags.init;

    uint element;
}

struct Image {
    private:
        VkImage _image;
        VkDeviceMemory _mem;
        VkImageView _view;
        uint _numLayers;
        Device* _device;
        VkImageLayout _layout = VK_IMAGE_LAYOUT_UNDEFINED;

    public:
        VkImage     handle() { return _image;   }
        VkImageView view()   { return _view;    }
        alias view this;

        void setLayout(VkImageLayout dstLayout) {
            _device.executeCommandBuf((VkCommandBuffer cmd){
                VkImageMemoryBarrier barrier;
                VkPipelineStageFlags before;
                VkPipelineStageFlags after;

                with (barrier) {
                    oldLayout           = VK_IMAGE_LAYOUT_UNDEFINED;
                    newLayout           = dstLayout;
                    srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                    dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                    image               = _image;
                    with (subresourceRange) {
                        aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
                        baseMipLevel   = 0;
                        levelCount     = 1;
                        baseArrayLayer = 0;
                        layerCount     = _numLayers;
                    }

                    if (_layout == VK_IMAGE_LAYOUT_UNDEFINED && dstLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
                        srcAccessMask = 0;
                        dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

                        before = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                        after  = VK_PIPELINE_STAGE_TRANSFER_BIT;
                    } else if (_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && dstLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
                        srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                        dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                        before = VK_PIPELINE_STAGE_TRANSFER_BIT;
                        after  = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
                    } else {
                        throw new Exception(format("Unkown image layout transition %s -> %s", _layout, dstLayout));
                    }
                }
                vkCmdPipelineBarrier(cmd, before, after, 0, 0, null, 0, null, 1, &barrier);
            });

            _layout = dstLayout;
        }

        void loadNewImage(ref Device device, VkImageUsageFlags usageFlags, uint width, uint height, uint layers = 1) {
            this.destroy;
            _device = &device;
            assert(layers == 1 || layers == 6, "Invalid number of layers");

            //Select values to use in creation of the image object and image view object
            VkFormat chosenFormat;
            VkImageAspectFlags chosenAspect;

            if (usageFlags & VK_IMAGE_USAGE_SAMPLED_BIT) {
                chosenFormat = VK_FORMAT_R8G8B8A8_SRGB;
                chosenAspect = VK_IMAGE_ASPECT_COLOR_BIT;
            } else if (usageFlags & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) {
                chosenFormat = VK_FORMAT_D32_SFLOAT;
                chosenAspect = VK_IMAGE_ASPECT_DEPTH_BIT;
            }

            //Create the image object
            VkImageCreateInfo imageInfo;
            with (imageInfo) {
                sType         = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                flags         = (layers == 6)*VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
                imageType     = VK_IMAGE_TYPE_2D;
                extent.width  = width;
                extent.height = height;
                extent.depth  = 1;
                mipLevels     = 1;
                arrayLayers   = layers;
                format        = chosenFormat;
                tiling        = VK_IMAGE_TILING_OPTIMAL;
                initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                usage         = usageFlags;
                sharingMode   = VK_SHARING_MODE_EXCLUSIVE;
                samples       = VK_SAMPLE_COUNT_1_BIT;
            }
            vkCreateImage(device, &imageInfo, null, &_image);
            enforce(_image, "Failed to create Vulkan image");

            //Create and bind a memory allocation for that image object
            VkMemoryRequirements requirements;
            vkGetImageMemoryRequirements(device, _image, &requirements);

            VkMemoryAllocateInfo allocateInfo;
            with (allocateInfo) {
                sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
                allocationSize  = requirements.size;
                memoryTypeIndex = device.findIdealMemTypes(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)[0];
            }
            vkAllocateMemory(device, &allocateInfo, null, &_mem);
            enforce(_mem, "Failed to allocate memory");
            vkBindImageMemory(device, _image, _mem, 0);

            //Create an image view for the image
            VkImageViewCreateInfo imageViewInfo;
            with (imageViewInfo) {
                sType      = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                image      = _image;
                format     = chosenFormat;
                viewType   = layers == 6 ? VK_IMAGE_VIEW_TYPE_CUBE : VK_IMAGE_VIEW_TYPE_2D;
                components = VkComponentMapping(VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY);
                with (subresourceRange) {
                    aspectMask     = chosenAspect;
                    baseMipLevel   = 0;
                    levelCount     = 1;
                    baseArrayLayer = 0;
                    layerCount     = layers;
                } 
            }
            vkCreateImageView(device, &imageViewInfo, null, &_view); 
            enforce(view, "Failed to create image view");
            _numLayers = layers;
        }

        this(ref Device device, VkImageUsageFlags usageFlags, uint width, uint height, uint layers = 1) {
            loadNewImage(device, usageFlags, width, height, layers);
        }

        ~this() {
            if (_device) {
                vkDeviceWaitIdle(*_device);
                vkDestroyImage(*_device, _image, null);
                vkFreeMemory(*_device, _mem, null);
                vkDestroyImageView(*_device, _view, null);
            }
        }
}

struct Texture {
    private:
        Image _image;

    public:
        VkImageView viewHandle() { return _image.view; }
        alias viewHandle this;

        void loadNewImage(ref Device device, ZipArchive archive, string[] imagePath ...) {
            Buffer staging;
            VkExtent2D resolution;

            foreach (i,e; imagePath) {
                //Load the image data from the passed zip archive value
                auto rw = archive.expand(archive.directory[e]).sdlRW;
                SDL_Surface* surface = IMG_LoadPNG_RW(&rw);
                enforce(surface, format("Failed to load image file: %s", IMG_GetError.to!string));

                //Create an image object if this is the first image file
                staging.loadNewBuf(device, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, (cast(ImageElement*)surface.pixels)[0..surface.w*surface.h]);
                if (resolution != VkExtent2D(0,0)) {
                    assert(VkExtent2D(surface.w, surface.h) == resolution, "All image files to use for singular texture must have the same resolution");
                } else {
                    _image.loadNewImage(device, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, surface.w, surface.h, cast(uint)imagePath.length);
                    _image.setLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
                    resolution = VkExtent2D(surface.w, surface.h);
                }

                //Load the image data into the image object
                VkBufferImageCopy copy;
                with (copy) {
                    with (imageSubresource) {
                        aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
                        mipLevel       = 0;
                        baseArrayLayer = cast(uint)i;
                        layerCount     = 1;
                    }
                    imageExtent = VkExtent3D(surface.w, surface.h, 1);
                }
                device.executeCommandBuf((VkCommandBuffer cmd) => vkCmdCopyBufferToImage(cmd, staging, _image.handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy));
                SDL_FreeSurface(surface);
            }

            _image.setLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        }

        this(ref Device device, ZipArchive archive, string[] imagePath ...) {
            loadNewImage(device, archive, imagePath);
        }
}

enum CubeMapSide {
    Front,
    Back,
    Top,
    Down,
    Right,
    Left,
    All
}

Texture cubemap(ref Device device, ZipArchive archive, string[CubeMapSide] sides) {
    string[6] sideStr = sides.get(CubeMapSide.All, null);
    foreach (i,e; sides) {
        if (i != CubeMapSide.All) {
            sideStr[i] = e; 
        }
    }
    return Texture(device, archive, sideStr);
}
