module render.instance;

import std.conv,
       std.format,
       std.exception;
import erupted,
       erupted.vulkan_lib_loader;
import bindbc.sdl;

pragma(lib, "SDL2");
pragma(lib, "SDL2_image");
pragma(lib, "BindBC_SDL");
pragma(lib, "erupted");

package {
    VkInstance instance;

    string[] extensions = [];
    string[] validation = [];

    const(char*)* strToCSlice(string[] str) {
        const(char*)[] toReturn;
        toReturn.reserve(str.length);
        foreach (i; str) {
            toReturn ~= (i ~ '\0').ptr;
        }
        return toReturn.ptr;
    }

    void initInstance(SDL_Window* window) {
        if (!instance) {
            enforce(loadGlobalLevelFunctions, "Failed to initialise Vulkan");

            //Get the instance extensions required by SDL for window surface creation
            uint numExtensions;
            SDL_Vulkan_GetInstanceExtensions(window, &numExtensions, null);
            const(char)*[] sdlExt;
            sdlExt.length = numExtensions;
            SDL_Vulkan_GetInstanceExtensions(window, &numExtensions, sdlExt.ptr);

            foreach (i; sdlExt) {
                extensions ~= i.to!string;
            }

            //Check to make sure that all extensions are available
            uint numExtension;
            vkEnumerateInstanceExtensionProperties(null, &numExtension, null);
            VkExtensionProperties[] extProperties;
            extProperties.length = numExtension;
            vkEnumerateInstanceExtensionProperties(null, &numExtension, extProperties.ptr);

            foreach (i; extensions) {
                bool found;
                foreach (ref e; extProperties) {
                    if (e.extensionName.ptr.to!string == i) {
                        found = true;
                        break;
                    }
                }
                enforce(found, format("Failed to find required extension %s", i));
            }

            //Check to make sure that all validation layers are available
            uint numLayer;
            vkEnumerateInstanceLayerProperties(&numLayer, null);
            VkLayerProperties[] layProperties;
            layProperties.length = numLayer;
            vkEnumerateInstanceLayerProperties(&numLayer, layProperties.ptr);

            foreach (i; validation) {
                bool found;
                foreach (ref e; layProperties) {
                    if (e.layerName.ptr.to!string == i) {
                        found = true;
                        break;
                    }
                }
                enforce(found, format("Failed to find required validation layer %s", i));
            }

            //Create a Vulkan instance
            VkInstanceCreateInfo instanceInfo;
            with (instanceInfo) {
                sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
                enabledExtensionCount   = cast(uint)extensions.length;
                ppEnabledExtensionNames = strToCSlice(extensions);
                enabledLayerCount       = cast(uint)validation.length;
                ppEnabledLayerNames     = strToCSlice(validation);
            }
            vkCreateInstance(&instanceInfo, null, &instance);
            enforce(instance, "Failed to create Vulkan instance");
            loadInstanceLevelFunctions(instance);
            loadDeviceLevelFunctions(instance);
        }
    }

    static this() {
        enforce(!SDL_Init(SDL_INIT_VIDEO), format("Failed to initialise SDL video subsystem: %s", SDL_GetError.to!string));
        enforce(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG, format("Failed to initialise SDL_Image: %s", IMG_GetError.to!string));
    }

    static ~this() {
        if (instance) {
            vkDestroyInstance(instance, null);
        }
        IMG_Quit;
        SDL_Quit;
    }
}
