import bindbc.sdl;

SDL_RWops sdlRW(const void[] slice) {
    //Creates an SDL_RWops value which can be used with extension libraries to allow them to access arbitrary array data
    SDL_RWops toReturn;
    with (toReturn) {
        seek = function long (SDL_RWops* rw, long offset, int mode) {
            //Change offset in slice array data
            switch (mode) {
                case 0 : rw.hidden.mem.here = cast(ubyte*)offset; break;
                case 1 : rw.hidden.mem.here += offset; break;
                case 2 : rw.hidden.mem.here = cast(ubyte*)(rw.hidden.mem.stop - offset); break;
                default: return -1;
            }
            return cast(long)rw.hidden.mem.here;
        };
        read = function size_t (SDL_RWops* rw, void* ptr, size_t size, size_t maxnum) {
            //Read a number of bytes from the slice array data
            auto slice = rw.hidden.mem.base[0..cast(size_t)rw.hidden.mem.stop];
            auto len   = size*maxnum;
            (cast(ubyte*)ptr)[0..len] = rw.hidden.mem.base[cast(size_t)rw.hidden.mem.here..cast(size_t)rw.hidden.mem.here+len];
            rw.seek(rw, len, 1);
            return maxnum;
        };
        hidden.mem.base = cast(ubyte*)slice.ptr;
        hidden.mem.stop = cast(ubyte*)slice.length;
    }
    return toReturn;
}
