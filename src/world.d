import std.math,
       std.conv,
       std.file,
       std.stdio,
       std.format,
       std.random,
       std.algorithm,
       std.exception,
       std.parallelism;
import math.shapes,
       math.vector;
import chunk;
import entity;
import render.device,
       render.model,
       render.pipeline;

class World {
    protected:
        uint seed;

        //Initial terrain generation function, primarily used to create it's shape
        abstract void genChunk(Chunk chunk, ivec3 chunkCoord);

        //Secondary terrain generation stage which cannot be multithreaded due to dependence on other chunks
        abstract void postGen(Chunk chunk, ivec3 chunkCoord);

    private final:
        File _file;
        string _path;
        Chunk[ivec3] _chunks;
        Chunk[ivec3] _inGenChunks;
        Chunk[ivec3] _newChunks;
        Entity[] _entities;
        Task!(run, void delegate())* _genTask;
        double _time = 1;

        enum fileMagic = "MANTLEVER0";

    protected final:
        string state()(auto ref ivec3 chunkPos) {
            if (chunkPos in _chunks) {
                return "generated";
            } else if (chunkPos in _inGenChunks) {
                return "in-generation";
            } else if (chunkPos in _newChunks) {
                return "generated-unloaded";
            } else {
                return "unloaded";
            }
        }

        bool loaded()(auto ref ivec3 pos) {
            return pos in _chunks || pos in _newChunks || pos in _inGenChunks;
        }

        void sphere()(auto ref dvec3 block, int chunkRadius, void delegate(ref ivec3) op) {
            auto key = block.toChunkCoord;

            //Find an area of chunks as specified by the arguments
            foreach (x; key.x - chunkRadius .. key.x + chunkRadius + 1) {
                foreach (y; key.y - chunkRadius .. key.y + chunkRadius + 1) {
                    foreach (z; key.z - chunkRadius .. key.z + chunkRadius + 1) {
                        //For every chunk in this area, check if the chunk is within a sphere given by the passed radius
                        auto pos = ivec3(cast(int)x,cast(int)y,cast(int)z);
                        if (((pos - key)^^2)[].sum <= chunkRadius^^2) {
                            op(pos);
                        }
                    }
                }
            }
        }

        vec3 curSkyLight(double time) {
            double x = 1 - abs(time - 0.5)*2;
            return vec3(x,x,x);
        }

        vec3 blockLight()(auto ref dvec3 block, double time) {
            vec3 toReturn = getBlock(block).color;
            foreach (i, ref e; toReturn) {
                e = max(curSkyLight(time)[i]*getBlock(block).skyLight, e);
            }
            return toReturn;
        }

        void calcLighting(TexVertElement[] vertices) {
            foreach (ref vert; vertices) {
                vert.skyLight = 0;
                vert.color = 0;
                uint numTrans;

                foreach (x; -1..1) {
                    foreach (z; -1..1) {
                        foreach (y; -1..1) {
                            //For every surrounding block find the maximum color components to use
                            auto block = dvec3(vert.xyz.x + x, vert.xyz.y + y, vert.xyz.z + z);
                            foreach (i,e; getBlock(block).color) {
                                vert.color[i] = max(vert.color[i], e);
                            }

                            numTrans += !getBlock(block).type.isOpaque;

                            //Find the maximum skylight also
                            vert.skyLight = max(vert.skyLight, getBlock(block).skyLight);
                        }
                    }
                }

                //Perform a basic form of ambient occlusion, where more surrounding non-opaque blocks means the vertex has brighter lighting
                vert.skyLight *= 0.4 + clamp(numTrans/8.0, 0, 4/8.0);
                vert.color    *= 0.4 + clamp(numTrans/8.0, 0, 4/8.0);
            }
        }

        void genModel(Chunk chunk, dvec3 offset, double time) {
            bool toInsert(ref Block a, ref Block b) {
                return !b.type.isOpaque && !(a.type == BlockType.Water && b.type == BlockType.Water);
            }

            void addIndices(TexVertElement[] vertices, ref uint[] indices) {
                indices ~= [cast(uint)(vertices.length-4), cast(uint)(vertices.length-3), cast(uint)(vertices.length-2), cast(uint)(vertices.length-2), cast(uint)(vertices.length-3), cast(uint)(vertices.length-1)];
            }

            with (chunk) {
                foreach (int x, ref xBlockTypes; blocks) {
                    foreach (int z, ref zBlockTypes; xBlockTypes) {
                        foreach (int y, ref yBlock; zBlockTypes) {
                            //Do not add faces for air blocks, or faces which are obscured
                            if (yBlock.type != BlockType.Air) {
                                auto vertices = yBlock.type.isOpaque ? &modelVert : &transVert;
                                auto indices  = yBlock.type.isOpaque ? &modelIndex : &transIndex;
                                auto height   = yBlock.type == BlockType.Water ? 0.9f : 1.0f;
                                auto block    = getBlock(offset+dvec3(x-1,y,z));

                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,-0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,+0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,+0.5f,-0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                } 
                                block = getBlock(offset+dvec3(x+1,y,z));
                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,-0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,+0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,+0.5f,+0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                }
                                block = getBlock(offset+dvec3(x,y-1,z));
                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,-0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,-0.5f,-0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                }
                                block = getBlock(offset+dvec3(x,y+1,z));
                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,+0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,+0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,+0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,+0.5f,+0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                }
                                block = getBlock(offset+dvec3(x,y,z-1));
                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,-0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,-0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(-0.5f,+0.5f,-0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+0.0f), yBlock.type, vec3(+0.5f,+0.5f,-0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                }
                                block = getBlock(offset+dvec3(x,y,z+1));
                                if (toInsert( yBlock, block )) {
                                    *vertices ~= [TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+0.0f  ,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,-0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+1.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(+0.5f,+0.5f,+0.5f)),
                                                  TexVertElement(vec3(offset.x+x+0.0f,offset.y+y+height,offset.z+z+1.0f), yBlock.type, vec3(-0.5f,+0.5f,+0.5f))];
                                    addIndices(*vertices, *indices);
                                    calcLighting((*vertices)[$-4..$]);
                                }
                            }
                        }
                    }
                }
            }
        }

        Model[2]* getModel()(Chunk chunk, ref Pipeline pipeline, auto ref dvec3 offset) {
            with (chunk) {
                if (!modelMade) {
                    //Create a task on another thread to generate a model for the given chunk
                    if (!taskSent) {
                        modelTask = task(&genModel, chunk, offset, _time);
                        taskPool.put(modelTask);
                        taskSent = true;
                    }

                    //If the chunk model was invalidated then it must be prepared immediately
                    if (invalid) {
                        modelTask.spinForce;
                    }

                    if (modelTask.done) {
                        //Move the model data to device local memory ready for rendering
                        modelData[0].loadNewModel(pipeline, TexVertElement.sizeof, modelVert, modelIndex);
                        modelData[0].buf.removeStaging;
                        modelData[1].loadNewModel(pipeline, TexVertElement.sizeof, transVert, transIndex);
                        modelData[1].buf.removeStaging;
                        modelMade = true;

                        modelVert = null;
                        transVert = null;
                        revalidate;
                        return &modelData;
                    }         
                } else {
                    return &modelData;
                }
            }

            return null;
        }

        void floodLight()(auto ref dvec3 pos, auto ref vec3 col, uint maxIntensity, uint intensity) {
            auto block = lazyGetBlock(pos);
            if (block && !block.type.isOpaque && block.color[].sum != float.infinity && intensity > 0) {
                //If this block is not opaque, was not flooded, and the remaining light intensity is greater than 0: continue flooding
                auto prev = block.color;
                block.color = float.infinity;
                foreach (i; 0..3) {
                    foreach (e; 0..2) {
                        auto toFlood = pos;
                        toFlood[i] += [-1,1][e];
                        floodLight(toFlood, col, maxIntensity, intensity-1);
                    }
                }

                //Once flooding has ended revert the previous block color and add the lighting
                block.color = prev;
                auto newCol = col * (intensity / cast(float)maxIntensity);
                foreach (i, ref e; block.color) {
                    e = max(e, newCol[i]);
                }
            }
        }

        void skyLight()(auto ref dvec3 pos) {
            auto next = pos;
            while (true) {
                next.y += 1;

                if (loaded(next.toChunkCoord)) {
                    if (getGeneralBlock(next).type.isOpaque) {
                        return;
                    }
                } else {
                    //If a higher chunk is unloaded then assume there is nothing blocking the skylight
                    next = pos;
                    while (loaded(next.toChunkCoord)) {
                        getGeneralBlock(next).skyLight = 1;
                        next.y += 1;
                    }
                    //floodLight(pos,color,5,5);
                    return;
                }
            }
        }

        ref Block getGenBlock()(auto ref dvec3 pos) {
            //Returns a value representing a block in an in-generation chunk
            auto block = pos.toChunkBlock;
            auto chunk = pos.toChunkCoord;
            assert(chunk in _inGenChunks, format("Attempt to obtain block at %s whose chunk is not in generation (%s)", pos, state(chunk)));
            return _inGenChunks[chunk][block.x][block.z][block.y];
        }

        ref Block getGeneralBlock()(auto ref dvec3 pos) {
            //Returns a value representing a block in any type of chunk
            auto block = pos.toChunkBlock;
            auto chunk = pos.toChunkCoord;
            if (chunk in _chunks) {
                return _chunks[chunk][block.x][block.z][block.y];
            } else if (chunk in _newChunks) {
                return _newChunks[chunk][block.x][block.z][block.y];
            } else if (chunk in _inGenChunks) {
                return _inGenChunks[chunk][block.x][block.z][block.y];
            }
            assert(false, format("Attempt to access block at %s whose chunk is unloaded", pos));
        }

        this() {}

    public final:
        double time() { return _time; }

        ref Block getBlock()(auto ref dvec3 pos) {
            //Returns a value representing a block in a generated chunk
            auto block = pos.toChunkBlock;
            auto chunk = pos.toChunkCoord;
            assert(chunk in _chunks, format("Failed to obtain block at %s whose chunk is not loaded (%s)", pos, state(chunk)));
            return _chunks[chunk][block.x][block.z][block.y];      
        }

        Chunk getChunk()(auto ref dvec3 pos) {
            return _chunks[pos.toChunkCoord];
        }

        Block* lazyGetBlock()(auto ref dvec3 pos) {
            auto block = pos.toChunkBlock;
            auto chunk = pos.toChunkCoord;

            if (chunk !in _chunks) {
                return null;
            } else {
                return &_chunks[chunk][block.x][block.z][block.y];
            }
        }

        void placeBlock()(auto ref dvec3 pos, BlockType type) {
            getBlock(pos).type = type;
            if (type.lightIntensity) {
                floodLight(pos, vec3(uniform01,uniform01,uniform01)/*type.lightColor*/, type.lightIntensity, type.lightIntensity);
            }
        }

        void breakBlock()(auto ref dvec3 pos) {
            getBlock(pos).type = BlockType.Air;
            skyLight(pos);
        }

        dvec3 moveCollide()(auto ref AABB!() b, auto ref dvec3 velo) {
            //Find the area of the world to do collision detection tests in
            AABB!() worldBox = b + velo;
            foreach (i; 0..3) {
                worldBox.extent[i] += worldBox.pos[i] - floor(worldBox.pos[i]);
                worldBox.pos[i]     = floor(worldBox.pos[i]);
                worldBox.extent[i]  = ceil(worldBox.extent[i]);
            }

            //Find the earliest block collided with
            dvec3 toCorrect = 0;
            dvec3 earliestEntry;
            dvec3 earliestCorrection;
            auto entryTime = double.infinity;

            with (worldBox) foreach (x; pos.x .. pos.x + extent.x) {
                foreach (y; pos.y .. pos.y + extent.y) {
                    foreach (z; pos.z .. pos.z + extent.z) {
                        auto block = &getBlock(dvec3(x,y,z));
   
                        if (block.type != BlockType.Air && block.type != BlockType.Water) {
                            double thisEntryTime;
                            auto correction = b.moveCollide(AABB!()(dvec3(x,y,z), dvec3(1,1,1)), velo, thisEntryTime);
                            if (thisEntryTime >= 0 && thisEntryTime < entryTime) {
                                entryTime = thisEntryTime;
                                earliestCorrection = correction;
                                earliestEntry = dvec3(x,y,z);
                            }
                        }
                    }
                }
            }

            //If a block was collided with, then simulate the AABB sliding off the collided face
            if (entryTime < 1) {
                dvec3 nextVelo = velo * (1-entryTime);
                foreach (i, ref e; earliestCorrection) {
                    if (e != 0) {
                        nextVelo[i] = 0;

                        //Make the response vector overcompensate to avoid AABBs colliding due to floating point rounding errors
                        if (e > 0) {
                            e += 0.001;
                        } else {
                            e -= 0.001;
                        }
                    }
                }
                toCorrect += earliestCorrection
                            +moveCollide(AABB!()(b.pos + velo*entryTime, b.extent), nextVelo);         
            }
            return toCorrect;
        }

        void addEntity(Entity entity) {
            _entities ~= entity;
        }

        void update(double delta) {
            //Move entities
            foreach (ref i; _entities) {
                auto correction = moveCollide(i.box, i.velo*delta);
                i.update(delta, correction);
            }

            //Update the time, every second is equivalent to two minutes
            _time += delta*0.0025; //(120 / (60*60*24));
            if (_time > 1) {
                _time = 0;
            }
        }

        void updateChunks() {
            //Load a collection of newly completed chunks as generated chunks
            if (_genTask.done) {
                foreach (i,e; _newChunks) {
                    _chunks[i] = e;
                }
                _newChunks = null;
            }
        }

        void loadArea(dvec3 block, int chunkRadius, bool synchronous = true) {
            //Dispatch a worker thread task that will generate a spherical area of the world
            if (!_genTask || _genTask.done) {
                _genTask = task({
                    Task!(run, void delegate(Chunk, vec!(3LU, int)), Chunk, vec!(3LU, int))*[] tasks;
                    tasks.reserve((chunkRadius*2 + 9)^^3);

                    //Load every unloaded chunk in the area specified (with an added area for chunks which may influence the given area)
                    sphere(block, chunkRadius+4, (ref ivec3 pos) {
                        if (!loaded(pos)) {
                            //Generate the chunk
                            auto chunk = new Chunk;
                            _inGenChunks[pos] = chunk;
                            tasks ~= task(&genChunk, chunk, pos);
                            taskPool.put(tasks[$-1]);
                        }
                    });
                    foreach (ref i; tasks) {
                        i.spinForce;
                    }

                    //Apply the secondary chunk generation stage to all chunks but the outermost layer
                    sphere(block, chunkRadius+2, (ref ivec3 pos) {
                        if (pos in _inGenChunks) {
                            auto chunk = _inGenChunks[pos];
                            if (!chunk.postGen) {
                                postGen(chunk, pos);
                                chunk.postGen = true;
                            }
                        }
                    });

                    //Add the chunks in the specified area as complete
                    sphere(block, chunkRadius, (ref ivec3 pos) {
                        if (pos !in _chunks && pos !in _newChunks) {
                            _newChunks[pos] = _inGenChunks[pos];
                            _inGenChunks.remove(pos);
                        }
                    });
                });

                //Dispatch the task and wait if the caller specified it should be synchronous
                taskPool.put(_genTask);
                if (synchronous) {
                    _genTask.spinForce;
                    updateChunks;
                }
            }
        }

        Model[2]*[] areaModels()(auto ref dvec3 block, int chunkRadius, ref Pipeline pipeline) {
            Model[2]*[] models;
            models.reserve((chunkRadius*2 + 1)^^3);

            sphere(block, chunkRadius, (ref ivec3 pos) {
                //For every chunk in the area specified, check if they and all adjacent chunks are loaded
                if (pos in _chunks) {
                    foreach (x; -1..2) {
                        foreach (z; -1..2) {
                            foreach (y; -1..2) {
                                if (pos + ivec3(x,y,z) !in _chunks) {
                                    return;
                                }
                            }
                        }
                    }

                    //If they are then obtain models if available
                    auto model = getModel(_chunks[pos], pipeline, dvec3(pos.x*Chunk.size, pos.y*Chunk.size, pos.z*Chunk.size));
                    if (model) {
                        models ~= model;
                    }
                }
            });

            return models;
        }

        Model*[] entityModels(ref Pipeline pipeline) {
            Model*[] models;
            models.reserve(_entities.length);

            foreach (i; _entities) {
                models ~= &(i.model.build(pipeline));
            }

            return models;
        }

        void invalidate()(auto ref dvec3 block) {
            //Invalidate a chunk model containing a block, and adjacent chunk models if needed
            getChunk(block).invalidate;

            auto offsets = block.toChunkBlock;
            auto light   = max(1, getBlock(block).type.lightIntensity);
            foreach (i,e; offsets) {
                auto toInv = block;
                if (e < light) {
                    toInv[i] -= light;
                    getChunk(toInv).invalidate;
                } else if (e > Chunk.size-(1+light)) {
                    toInv[i] += light;
                    getChunk(toInv).invalidate;
                }
            }
        }
    
        this(string name, uint seed) {
            //Create a directory for the new world
            this.seed = seed;
            _path = "world/" ~ name;
            assert(!_path.exists, "World with given name already exists in world directory");

            //Create a file storing the metadata for the world
            _file = File(_path, "w+");
            _file.write(fileMagic);
            _file.rawWrite([seed]);
            _file.rawWrite([_time]);
        }

        this(string name) {
            if (exists("world/" ~ name)) {
                //Open an existing world, ensuring that the magic number in the metadata file is correct
                _path = "world/" ~ name;
                _file = File(_path, "w+");
                char[fileMagic.length] magic;
                _file.rawRead(magic);
                enforce(magic != fileMagic, "World file opened does not have a correct header");

                //Load the seed and time
                _file.rawWrite((&seed)[0..1]);
                _file.rawWrite((&_time)[0..1]);

                this();
            } else {
                this(name, unpredictableSeed);
            }
        }

        ~this() {
            foreach (i; _chunks) {
                //object.destroy isnt working bruh
                i.__dtor;
            }
            foreach (i; _newChunks) {
                //object.destroy isnt working bruh
                i.__dtor;
            }
            foreach (i; _inGenChunks) {
                //object.destroy isnt working bruh
                i.__dtor;
            }
            foreach (i; _entities) {
                //object.destroy isnt working bruh
                i.__dtor;
            }
        }
}
