import chunk;
import world;
import math.noise,
       math.vector;
import std.algorithm;

class RegularWorld : World {
    protected:
        enum treeChance = 0.006;
        enum pigChance  = 0.00001;

        override void genChunk(Chunk chunk, ivec3 chunkCoord) {
            //Generate the basic terrain shape using stone blocks
            foreach (x; 0..Chunk.size) {
                foreach (z; 0..Chunk.size) with (BlockType) {
                    auto global = dvec3(chunkCoord.x*Chunk.size + x, 0, chunkCoord.z*Chunk.size + z);

                    foreach (y; 0..Chunk.size) {
                        //Calculate a density threshold above which stone will generate, based on the global block height
                        global.y = chunkCoord.y*Chunk.size + y;
                        auto threshold = global.y;
                        if (threshold > 150) {
                            threshold = 1;
                        } else if (threshold < 0) {
                            threshold = 0.2;
                        } else if (threshold >= 0) {
                            threshold = 0.2 + (threshold/150)*0.8;
                        } else if (threshold < 100) {
                            threshold = 0.4;
                        }

                        //Calculate a density for each block using 3D perlin noise
                        if (threshold < 1) {
                            auto density = clamp(perlin(seed,global.x*0.01,global.y*0.01,global.z*0.01) + 
                                                 perlin(seed+1,global.x*0.05,global.y*0.05,global.z*0.05) * 0.1,
                                                 0, 1);
                            if (density > threshold) {
                                chunk[x][z][y] = Block(Stone);
                            }
                        }
                    }
                }
            }
        }

        override void postGen(Chunk chunk, ivec3 chunkCoord) {
            //Make final adjustments to the terrain
            foreach (x; 0..Chunk.size) {
                foreach (z; 0..Chunk.size) {
                    auto global = dvec3(chunkCoord.x*Chunk.size + x, 0, chunkCoord.z*Chunk.size + z);
                    auto heat   = perlin(seed,global.x,global.z);
                    
                    foreach (y; 0..Chunk.size) {
                        global.y = chunkCoord.y*Chunk.size + y;
                        if (chunk[x][z][y].type == BlockType.Stone && !getGeneralBlock(global + dvec3(0,1,0)).type.isOpaque) {
                            if (global.y > 0) { 
                                //Choose surface blocks to use based on biome
                                BlockType surface;
                                BlockType under;
                                if (heat > 0.3) {
                                    surface = BlockType.Sand;
                                    under   = BlockType.Sand;
                                } else {
                                    surface = global.y > 25 ? BlockType.Grass : BlockType.Sand;
                                    under   = global.y > 25 ? BlockType.Dirt  : BlockType.Sand;
                                }

                                //Place the surface block and a layer of blocks underneath
                                chunk[x][z][y] = Block(surface);
                                auto next = global;
                                foreach (i; 0..10) {
                                    next.y -= 1;
                                    auto block = &getGenBlock(next);
                                    if (block.type == BlockType.Stone) {
                                        block.type = under;
                                    } else {
                                        break;
                                    }
                                }

                                //Randomly place trees
                                if (surface == BlockType.Grass) {
                                    if (white(seed,global.x,global.y,global.z) < treeChance) {
                                        //Insert a log
                                        next = global;
                                        auto height = 3 + (white(seed+1,global.x,global.y,global.z)*4);
                                        foreach (i; 0..height) {
                                            next.y += 1;
                                            getGenBlock(next).type = BlockType.OakLog;
                                        }

                                        //Insert a semi-sphere leaf area
                                        auto radius = 3 + (white(seed+2,global.x,global.y,global.z)*2);
                                        auto leaves = global;
                                        leaves.y += height;
                                        foreach (circleX; leaves.x - radius .. leaves.x + radius + 1) {
                                            foreach (circleZ; leaves.z - radius .. leaves.z + radius + 1) {
                                                foreach (circleY; leaves.y .. leaves.y + radius + 1) {
                                                    if (((dvec3(circleX,circleY,circleZ) - leaves)^^2)[].sum <= radius^^2) {
                                                        auto block = &getGenBlock(dvec3(circleX,circleY,circleZ));
                                                        if (block.type == BlockType.Air) {
                                                            block.type = BlockType.OakLeaves;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else if (white(seed+1,global.x,global.y,global.z) < pigChance) {
                                        //_entities ~= new Pig(global + dvec3(0,1,0));
                                    }
                                }
                            }

                            //Add sky lighting
                            skyLight(global + dvec3(0,1,0));
                        }
                    }
                }
            }
        }

    public:
        this(string name, uint seed) { super(name, seed); }
        this(string name) { super(name); }
}

class FlatWorld : World {
    protected:
        override void genChunk(Chunk chunk, ivec3 chunkCoord) {
            auto grassPos = 0 - chunkCoord.y*Chunk.size;
            auto dirtPos  = grassPos-20;

            foreach (x; 0..Chunk.size) {
                foreach (z; 0..Chunk.size) with (BlockType) {
                    //For each block x,z area, generate a layer of grass with 20 dirt blocks and infinite stone underneath
                    if (grassPos >= 0 && grassPos < Chunk.size) {
                        chunk[x][z][grassPos] = Block(Grass);
                    }
                    if (dirtPos < Chunk.size) {
                        chunk[x][z][clamp(dirtPos,0,Chunk.size)..clamp(grassPos,0,Chunk.size)] = Block(Dirt);
                    }
                    if (dirtPos > 0) {
                        chunk[x][z][0..clamp(dirtPos,0,Chunk.size)] = Block(Stone);
                    }
                }
            }
        }

        override void postGen(Chunk chunk, ivec3 chunkCoord) {}

    public:
        this(string name, uint seed) { super(name, seed); }
        this(string name) { super(name); }
}
