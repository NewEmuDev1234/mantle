module audio.sound;

import bindbc.sdl;
import std.zip;
import util;

struct Sound {
    private:
        Mix_Chunk* _audio;

    public:
        void play() {
            Mix_PlayChannel(-1, _audio, 0);
        }

        @disable this();

        this(ZipArchive archive, string file) {
            auto rw = archive.expand(archive.directory[file]).sdlRW;
            _audio  = Mix_LoadWAV_RW(&rw, 0);
        }

        ~this() {
            Mix_FreeChunk(_audio);
        }
}
