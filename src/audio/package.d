module audio;

public import audio.sound;

import std.conv,
       std.format,
       std.exception;
import bindbc.sdl;

pragma(lib, "SDL2_mixer");

static this() {
    enforce(!Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096), format("Failed to initialise SDL_Mixer: %s", Mix_GetError.to!string));
}

static ~this() {
    Mix_Quit;
}
