module math.shapes;

import std.math,
       std.algorithm;
import math.vector;

struct AABB(size_t dim = 3, T = double) {
    vec!(dim,T) pos = 0;
    vec!(dim,T) extent = 0;

    invariant {
        foreach (i; extent) {
            assert(i >= 0, "AABB extents must be positive");
        }
    }

    vec!(dim,T) moveCollide()(auto ref AABB!(dim,T) b, auto ref vec!(dim,T) velo, out T entry) {
        if (this + velo !in b) {
            vec!(dim,T) toReturn = 0;
            return toReturn;
        }

        //Find the collision distances on each axis as a proportion of the velocity vector components
        T[dim] entryDist;
        T[dim] exitDist;
        foreach (i; 0..dim) {
            if (velo[i] > 0) {
                entryDist[i] = (b.pos[i] - (pos[i]+extent[i])) / velo[i];
                exitDist [i] = ((b.pos[i]+b.extent[i]) - pos[i]) / velo[i];
            } else if (velo[i] < 0) {
                entryDist[i] = ((b.pos[i]+b.extent[i]) - pos[i]) / velo[i];
                exitDist [i] = (b.pos[i] - (pos[i]+extent[i])) / velo[i];
            } else if (velo[i] == 0) {
                entryDist[i] = -T.infinity;
                exitDist [i] = +T.infinity;
            }
        }

        //Detect if a collision occured
        int numNegative;
        bool outOfRange;
        foreach (i; entryDist) {
            if (i < 0) {
                numNegative += 1;
            }
            if (i > 1) {
                outOfRange = true;
            }
        }

        if (entry > exitDist.reduce!min || numNegative == dim || outOfRange) {
            entry = T.infinity;
            vec!(dim,T) toReturn = 0;
            return toReturn;
        } else {
            //If a collision occured then create a correction vector that creates a slide effect against the collided face
            size_t lastCollided;
            foreach (i,e; entryDist) {
                if (e > entryDist[lastCollided]) {
                    lastCollided = i;
                }
            }

            vec!(dim,T) toReturn = 0;
            if (velo[lastCollided] > 0) {
                toReturn[lastCollided] -= (pos[lastCollided]+extent[lastCollided]+velo[lastCollided]) - b.pos[lastCollided];
            } else if (velo[lastCollided] < 0) {
                toReturn[lastCollided] += (b.pos[lastCollided]+b.extent[lastCollided]) - (pos[lastCollided]+velo[lastCollided]);
            }

            entry = entryDist.reduce!max;
            return toReturn;
        }
    }

    bool opBinary(string op)(auto ref AABB!(dim,T) b) if (op == "in") {
        foreach (i; 0..dim) {
            if (pos[i] > b.pos[i]+b.extent[i] || pos[i]+extent[i] < b.pos[i]) {
                return false;
            }
        }
        return true;
    }

    AABB!dim opBinary(string op)(vec!(dim,T) b) if (op == "+") {
        //Adds a velocity vector to the bounding box, so that the bounding box covers the whole possible area of collision
        AABB!dim toReturn = this;
        foreach (i; 0..dim) with (toReturn) {
            if (b[i] > 0) {
                extent[i] += b[i];
            } else if (b[i] < 0) {
                extent[i] -= b[i];
                pos[i] += b[i];
            }
        }
        return toReturn;
    }

    void opAssignOp(string op)(vec!(dim,T) b) {
        mixin("this = this ", op, " b;");
    }
}
