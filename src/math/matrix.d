module math.matrix;

import std.math;
import math.vector;

struct mat(size_t row, size_t col = row, T = float) if (row >= 1 && row <= 4 && col >= 1 && col <= 4) {
    T[row][col] val;
    alias val this;

    mat!(col,bT,T) opBinary(string op, alias bT : mat!(col,bT,T))(mat!(col,bT,T) b) if (op == "*") {
        typeof(b) toReturn;

        foreach (y; 0..col) {
            foreach (x; 0..b.length) {
                T val = 0.0;
                foreach (i; 0..col) {
                    val += this[i][y] * b[x][i];
                }
                toReturn[x][y] = val;
            }
        }

        return toReturn;
    }

    vec!(col,T) opBinary(string op)(auto ref vec!(col,T) b) if (op == "*") {
        return vec!(col,T)( ( this * mat!(col,1,T)(b) )[0] );
    }

    this(T[row][] toAssign) in (toAssign.length == col) {
        val = toAssign; 
    }
    this()(auto ref vec!(row,T) toAssign) in (col == 1) {
        val[0] = toAssign;
    }
    this(T toAssign) { 
        val = toAssign;
    } 
}

alias mat2  = mat!2;
alias mat3  = mat!3;
alias mat4  = mat!4;
alias dmat2 = mat!(2,2,double);
alias dmat3 = mat!(3,3,double);
alias dmat4 = mat!(4,4,double);

T identity(T = mat4)() {
    T toReturn = T(0);
    foreach (i; 0..T.length) {
        toReturn[i][i] = 1;
    }
    return toReturn;
}

T translate(T = mat4, A)(A[] coord ...) {
    T toReturn = identity!T;
    foreach (i,e; coord) {
        toReturn[$-1][i] = e;
    }
    return toReturn;
}

T scaleX(T = mat4, A)(A scale) {
    T toReturn = identity!T;
    toReturn[0][0] = scale;
    return toReturn;
}

T scaleY(T = mat4, A)(A scale) {
    T toReturn = identity!T;
    toReturn[1][1] = scale;
    return toReturn;
}

T scaleZ(T = mat4, A)(A scale) {
    T toReturn = identity!T;
    toReturn[2][2] = scale;
    return toReturn;
}

T scale(T = mat4, A)(A scale) {
    T toReturn = identity!T;
    foreach (i; 0..3) {
        toReturn[i][i] = scale;
    }
    return toReturn;
}

T pitch(T = mat4, A)(A angle) {
    T toReturn = identity!T;
    toReturn[1][1] = cos(angle);
    toReturn[2][1] = -sin(angle);
    toReturn[1][2] = sin(angle);
    toReturn[2][2] = cos(angle);
    return toReturn;
}

T yaw(T = mat4, A)(A angle) {
    T toReturn = identity!T;
    toReturn[0][0] = cos(angle);
    toReturn[2][0] = -sin(angle);
    toReturn[0][2] = sin(angle);
    toReturn[2][2] = cos(angle);
    return toReturn;
}

T rotation(T = mat4, A)(vec!(2,A) orientation) {
    return pitch!T(orientation.pitch) * yaw!T(orientation.yaw);
}

T perspective(T = mat4, A)(A fov, A zNear, A zFar) {
    T toReturn = identity!T;
    toReturn[$-2][$-2] = sqrt(1/(zFar-zNear));
    toReturn[$-2][$-1] = tan(fov/2.0);
    toReturn[$-1][$-1] = 0.0;
    return toReturn * translate!T(0.0, 0.0, -zNear);
}
