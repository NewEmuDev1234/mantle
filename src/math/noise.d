module math.noise;

import math.vector,
       math.matrix;
import std.math,
       std.algorithm;

float white(uint seed, float[] x...) out (ret; ret >= 0 && ret <= 1) {
    size_t total;

    foreach (i,e; x) {
        total = (i+e).hashOf(total);
    }
    total += seed.hashOf(total);

    return cast(float)total / size_t.max;
}

float perlin(uint seed, float x, float y) out (ret; ret >= 0 && ret <= 1) {
    //Find the random vectors for each vertex of the box that the given point is in
    float[2][2] dot;
    foreach (vertX; 0..2) {
        foreach (vertY; 0..2) {
            //Find the dot product between each vertex vector and the position vector for the point used
            auto angle = white(seed, floor(x)+vertX, floor(y)+vertY) * PI * 2;
            dot[vertX][vertY] = (vec2(x,y) - vec2(floor(x)+vertX, floor(y)+vertY)).dot(vec2(cos(angle), sin(angle)));
        }
    }

    //Interpolate the final noise value
    float ease(float x) in (x >= 0 && x <= 1) { return clamp(6*(x^^5) - 15*(x^^4) + 10*(x^^3), 0, 1); }

    auto yMult = ease(y - floor(y));
    auto x0 = dot[0][0] * (1-yMult) + dot[0][1] * yMult;
    auto x1 = dot[1][0] * (1-yMult) + dot[1][1] * yMult;
    auto xMult = ease(x - floor(x));
    return ((x0 * (1-xMult) + x1 * xMult) + 0.5^^0.5 ) / 2*(0.5^^0.5);
}

float perlin(uint seed, float x, float y, float z) out (ret; ret >= 0 && ret <= 1) {
    //Find the random vectors for each vertex of the box that the given point is in
    float[2][2][2] dot;
    foreach (vertX; 0..2) {
        foreach (vertY; 0..2) {
            foreach (vertZ; 0..2) {
                //Find the dot product between each vertex vector and the position vector for the point used
                auto xyz = vec3(floor(x)+vertX, floor(y)+vertY, floor(z)+vertZ);
                auto vec = yaw!mat3(white(seed,seed,xyz.x,xyz.y,xyz.z) * PI * 2) * pitch!mat3(white(seed,seed,seed,xyz.x,xyz.y,xyz.z) * PI * 2) * vec3(0,0,1);
                dot[vertX][vertY][vertZ] = (vec3(x,y,z) - xyz).dot(vec);
            }
        }
    }

    //Interpolate the final noise value
    float ease(float x) in (x >= 0 && x <= 1) { return clamp(6*(x^^5) - 15*(x^^4) + 10*(x^^3), 0, 1); }

    auto xMult = ease(x - floor(x));
    auto yMult = ease(y - floor(y));
    auto zMult = ease(z - floor(z));

    float[2] sides;
    foreach (i; 0..2) {
        auto x0 = dot[0][0][i] * (1-yMult) + dot[0][1][i] * yMult;
        auto x1 = dot[1][0][i] * (1-yMult) + dot[1][1][i] * yMult;
        sides[i] = ((x0 * (1-xMult) + x1 * xMult) + 0.75^^0.5 ) / 2*(0.75^^0.5);
    }

    //The output range on these perlin noise functions is kind of fucked ngl idk why
    return sides[0] * (1-zMult) + sides[1] * zMult;
}
