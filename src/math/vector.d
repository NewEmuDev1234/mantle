module math.vector;

import std.math,
       std.array,
       std.algorithm;

struct vec(size_t n, T = float) if (n >= 2 && n <= 4) {   
    T[n] components;
    alias components this;

    T dot()(auto ref vec!(n,T) b) {
        return (this * b)[].sum;
    }

    T len() {
        T toReturn = 0;
        foreach (i; this) {
            toReturn += i^^2;
        }
        return cast(T)sqrt(cast(real)toReturn);
    }

    vec!(n,T) unit() {
        return this / len;
    }

    vec!(n,T) opBinary(string op)(auto ref vec!(n,T) b) {
        vec!(n,T) toReturn = this;
        mixin("toReturn[]", op, "= b[];");
        return toReturn;
    }

    vec!(n,T) opBinary(string op)(T b) {
        vec!(n,T) toReturn = this;
        mixin("toReturn[]", op, "= b;");
        return toReturn;
    }

    void opOpAssign(string op)(auto ref vec!(n,T) b) { mixin("this = this", op, "b;"); }
    void opOpAssign(string op)(T b)                  { mixin("this = this", op, "b;"); }

    ref T pitch() { return components[0]; }
    ref T yaw  () { return components[1]; }
    ref T x() { return components[0]; }
    ref T y() { return components[1]; }
    static if (n >= 3) ref T z() { return components[2]; }
    static if (n == 4) ref T w() { return components[3]; }

    this(T[] toAssign ...) in (toAssign.length == n) {
        components = toAssign;
    }
    this(T toAssign) {
        components = toAssign;
    }
}

alias vec2  = vec!2;
alias vec3  = vec!3;
alias vec4  = vec!4;
alias dvec2 = vec!(2,double);
alias dvec3 = vec!(3,double);
alias dvec4 = vec!(4,double);
alias ivec2 = vec!(2,int);
alias ivec3 = vec!(3,int);
alias ivec4 = vec!(4,int);
