module math;

public import math.vector,
              math.shapes,
              math.noise,
              math.matrix;
