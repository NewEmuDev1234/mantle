import bindbc.sdl;
import chunk;
import worldgen;
import entity;
import math;
import audio;
import render;
import std.zip,
       std.file,
       std.conv,
       std.math,
       std.stdio,
       std.random,
       std.algorithm,
       std.datetime.stopwatch;

string[string] settings;

struct WorldUBO {
    align(16) mat4 transform;
    align(16) vec3 fogColor;
    align(8) double time;
}

void saveSettings() {
    File file = File("settings", "w");
    foreach (i,e; settings) {
        file.writeln(i, ":", e);
    }
}

void loadSettings() {
    if (!"settings".exists) {
        settings = [
            "renderDist":"8",
            "windowW":"-1",
            "windowH":"-1",
            "fullscreen":"true"
        ];
        saveSettings;
    } else {
        File file = File("settings", "r");
        while (!file.eof) {
            string setting;
            string val;
            file.readf("%s:%s\n", setting, val);
            settings[setting] = val;
        }
    }
}

void updateSky(ref ColVertElement[8] vertices, double time) {
    auto day     = [vec4(0.5f,0.8f,0.9f,1.0f), vec4(0.0f,0.7f,1.0f,1.0f)];
    auto evening = [vec4(1.0f,0.6f,0.3f,1.0f), vec4(0.0f,0.1f,0.3f,1.0f)];
    auto night   = [vec4(0.2f,0.2f,0.4f,1.0f), vec4(0.0f,0.1f,0.3f,1.0f)];

    //Find the upper and lower colors for the sky given the time
    vec4[2] overall;
    auto centerDist = abs(time - 0.5);
    if (centerDist < 0.25) {
        overall = day;
    } else if (centerDist < 0.35) {
        foreach (i, ref e; overall) {
            auto scale = (centerDist - 0.25) / 0.1;
            e = evening[i]*scale + day[i]*(1-scale);
        }
    } else {
        foreach (i, ref e; overall) {
            auto scale = (centerDist - 0.35) / 0.15;
            e = night[i]*scale + evening[i]*(1-scale);
        }
    }

    //Apply the colors to the vertices
    foreach (ref i; vertices) {
        i.rgb = i.xyz.y == 1 ? overall[0] : overall[1];
    }
}

void main() {
    auto resources = new ZipArchive(import("default.zip").dup);
    loadSettings;
    auto renderDist = settings["renderDist"].to!int;

    //Create a window
    Device device;
    auto window = Window(device, "Mantle", settings["windowW"].to!int, settings["windowH"].to!int, settings["fullscreen"].to!bool);

    //Create a world model and related resources for rendering
    Texture[8] image = [cubemap(device, resources, [CubeMapSide.All:"texture/air.png"]), cubemap(device, resources, [CubeMapSide.All:"texture/dirt.png"]), 
                        cubemap(device, resources, [CubeMapSide.All:"texture/grass.png", CubeMapSide.Top:"texture/grass_top.png"]),
                        cubemap(device, resources, [CubeMapSide.All:"texture/stone.png"]), cubemap(device, resources, [CubeMapSide.All:"texture/sand.png"]),
                        cubemap(device, resources, [CubeMapSide.All:"texture/water.png"]),
                        cubemap(device, resources, [CubeMapSide.All:"texture/log.png", CubeMapSide.Top:"texture/log_top.png", CubeMapSide.Down:"texture/log_top.png"]),
                        cubemap(device, resources, [CubeMapSide.All:"texture/leaves.png"])];
    auto worldPipe   = pipeline!(TexVertElement, WorldUBO)(window, true, resources, ["shader/vertregular.o", "shader/fragregular.o"], image);
    auto world       = new RegularWorld("Test");
    Model[2]*[] worldMod;
    Model*[] entityMod;

    //Create a skybox model to render
    auto skyPipe = pipeline!ColVertElement(window, true, resources, ["shader/vertsky.o", "shader/fragsky.o"]);
    auto skyMod  = getCube!ColVertElement;
    updateSky(skyMod, 0);
    auto sky  = Model(skyPipe, ColVertElement.sizeof, skyMod, cubeIndex, true);
    auto pass = RenderPass(window, true);

    //Trap the mouse cursor and enter the main loop
    SDL_SetRelativeMouseMode(SDL_TRUE);

    bool quit;
    auto sfx = Sound(resources, "sound/walk_grass.mp3");
    bool[SDL_Keycode] pressed;
    auto player = new Player(dvec3(0,45,0));
    auto chkTime = 1.0;
    auto lastStep = 0.0;

    StopWatch timer;
    timer.start;
    world.addEntity(player);
    world.loadArea(player.pos,renderDist);

    while (!quit) {
        //Handle SDL event queue
        SDL_Event event;
        while (SDL_PollEvent(&event)) switch (event.type) {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_MOUSEMOTION:
                //Move the camera orientation
                player.camOrient.pitch += event.motion.yrel*0.01;
                player.camOrient[0]     = player.camOrient[0].clamp(-PI/2, PI/2);
                player.camOrient.yaw   += event.motion.xrel*0.01;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    //Break a block
                    auto dir = yaw!dmat3(player.camOrient.yaw) * pitch!dmat3(player.camOrient.pitch) * dvec3(0,0,1) / 5;
                    dir.z *= -1;
                    auto cur = player.pos + dvec3(0,0.475,0) + dir;
                    foreach (i; 0..50) {
                        auto block = &world.getBlock(cur);
                        if (block.type != BlockType.Air) {
                            world.breakBlock(cur);
                            break;
                        }
                        cur += dir;
                    }
                    world.invalidate(cur);
                    chkTime = 1;
                } else if (event.button.button == SDL_BUTTON_RIGHT) {
                    //Place a block
                    auto dir = yaw!dmat3(player.camOrient.yaw) * pitch!dmat3(player.camOrient.pitch) * dvec3(0,0,1) / 5;
                    dir.z *= -1;
                    auto cur = player.pos + dvec3(0,0.475,0) + dir;
                    foreach (i; 0..50) {
                        if (world.getBlock(cur).type != BlockType.Air) {
                            cur -= dir;
                            world.placeBlock(cur, BlockType.OakLeaves);
                            break;
                        }
                        cur += dir;
                    }
                    world.invalidate(cur);
                    chkTime = 1;
                }
                break;
            case SDL_KEYDOWN:
                pressed[event.key.keysym.sym] = true;
                break;
            case SDL_KEYUP:
                pressed[event.key.keysym.sym] = false;
                break;
            default:
                break;
        }

        quit = quit || pressed.get(SDLK_q, false);

        //Change velocity based on keys pressed
        enum moveSpeed = 10;
        auto delta = timer.peek.total!"nsecs"/1e9;
        delta     = delta.clamp(0, 1.0/24);
        chkTime  += delta;
        lastStep += delta;
        timer.reset;

        player.keyboardMove(pressed.get(SDLK_w, false),
                            pressed.get(SDLK_a, false),
                            pressed.get(SDLK_s, false),
                            pressed.get(SDLK_d, false),
                            pressed.get(SDLK_SPACE, false));

        //Update the loaded chunks and chunk models every second
        if (chkTime >= 1) {
            world.updateChunks;
            world.loadArea(player.pos,renderDist,false);
            worldMod = world.areaModels(player.pos,renderDist,worldPipe);
            chkTime = 0;
        }

        entityMod = world.entityModels(worldPipe);
        pass.reset;
        foreach (i; worldMod) {
            pass.add((*i)[0]);
        }
        foreach (i; entityMod) {
            pass.add(*i);
        }
        pass.add(sky);
        foreach (i; worldMod) {
            pass.add((*i)[1]);
        }

        //Play sounds
        if (player.grounded && lastStep > 1/2.5 && (player.velo.x != 0 || player.velo.z != 0)) {
            sfx.play;
            lastStep = 0;
        }

        //Update world
        world.update(delta);
        if (player.hp == 0) {
            player.respawn;
        }

        //Get a swapchain image and update the corresponding transformation matrices
        window.reset;
        updateSky(skyMod, world.time);
        sky.data!ColVertElement[] = skyMod[];
        sky.pushStaging;
        pass.recreateFrame;

        with (worldPipe.ubo!WorldUBO) {
            time     = world.time;
            fogColor = vec3(skyMod[2].rgb.x, skyMod[2].rgb.y, skyMod[2].rgb.z);
        }

        worldPipe.ubo!WorldUBO.transform = perspective(PI/2, 1e-9, renderDist*Chunk.size) * 
                                           scaleX(window.resolution.height/float(window.resolution.width)) * 
                                           rotation(player.camOrient) * 
                                           translate(player.pos + dvec3(0,0.475,0)) *
                                           scale(-1);
        skyPipe.ubo!UBO.transform = perspective(PI/2, 1e-9, renderDist*Chunk.size) * 
                                    scaleX(window.resolution.height/float(window.resolution.width)) * 
                                    rotation(player.camOrient) * 
                                    scale(100) * 
                                    translate(vec3(-0.5,-0.5,-0.5));

        //Render and present to the swapchain image
        window.present(&pass);
        if (window.invalid) {
            window.recreate;
            skyPipe.recreate;
            worldPipe.recreate;
            pass.recreate;
        }
    }

    scope(exit) world.destroy;
}
