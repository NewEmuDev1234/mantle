import std.math,
       std.random,
       std.algorithm;
import render.model;
import math.shapes,
       math.vector,
       math.matrix;

enum GameMode {
    Creative,
    Survival
}

class Entity {
    protected:
        dvec3 _size;
        dvec3 _velo = 0;
        dvec2 _orient = 0;
        bool _grounded;
        MultiModel!() _model;
        
        enum moveSpeed = 10;

    public:
        dvec3 pos;

        final AABB!() box()   { return AABB!()(pos - (_size/2), _size); }
        final dvec3 velo()    { return _velo;                           }
        final bool grounded() { return _grounded;                       }
        final ref MultiModel!() model() { return _model;                }

        abstract void update(double delta, ref dvec3 correction) {
            //Move using the velocity vector. A collision response vector should be passed
            pos += _velo*delta + correction;
            _velo.y -= 9.80665*delta;

            if (velo.y != 0) {
                _grounded = false;
            }
            if (correction.y > 0) {
                _velo.y   = 0;
                _grounded = true;
            }

            //Update the overall model transformation matrix
            if (_model.transforms) {
                _model.transforms[0] = translate(pos) * rotation(_orient);
            }        
        }

        ~this() {
            _model.destroy;
        }
}

class Living : Entity {
    public:
        uint hp;
        GameMode mode;

        override void update(double delta, ref dvec3 correction) {
            //Apply fall damage based on y speed
            if (correction.y > 0 && mode == GameMode.Survival) {
                hp -= cast(uint)clamp(-velo.y - 10, 0, hp);
            }

            super.update(delta, correction);
        }
}

class Pig : Living {
    private:
        static MultiModel!() _pig;
        double _moveTime = 0;
        double _moveAngle = 0;
        bool _still = true;

    public:
        override void update(double delta, ref dvec3 correction) {
            super.update(delta, correction);

            //Jump if grounded and a block was collided with
            if (_grounded && (correction.x != 0 || correction.z != 0)) {
                _velo.y = moveSpeed/2;
                _grounded = false;
            }

            //Every few seconds, change from moving in a random direction and standing still
            _moveTime -= delta;
            if (_moveTime <= 0) {
                _still = !_still;
                _moveTime = 3 + uniform01 * 5;

                if (!_still) {
                    _moveAngle = uniform01 * PI * 2;
                    _orient.yaw = _moveAngle + (PI/2)*3;
                    _velo.x = cos(_moveAngle)*moveSpeed*0.1;
                    _velo.z = sin(_moveAngle)*moveSpeed*0.1;
                } else {
                    _velo = dvec3(0, _velo.y, 0);
                }
            }
        }

        this()(auto ref dvec3 init) {
            pos    = init;
            _size  = dvec3(1,1,1);
            _model = MultiModel!()(_pig);
            hp     = 15;
        }

        static this() {
            //Body
            auto pigBody = getCube!TexVertElement;
            foreach (ref i; pigBody) with (i) {
                xyz -= 0.5;
                textureCoord = xyz;

                xyz.x *= 0.8;
                xyz.y *= 0.7;

                textureIndex = 1;
                color = 1;
            }

            //Head
            auto pigHead = getCube!TexVertElement;
            foreach (ref i; pigHead) with (i) {
                xyz -= 0.5;
                textureCoord = xyz;

                xyz *= 0.5;
                xyz.z += 0.5;
                xyz.y += 0.5;

                textureIndex = 1;
                color = 1;
            }

            //Legs
            TexVertElement[8][4] pigLegs;
            mat4[4] pigLegTransform;
            foreach (i, ref e; pigLegs) {
                e = getCube!TexVertElement;
                foreach (ref f; e) with (f) {
                    xyz -= 0.5;
                    textureCoord = xyz;

                    xyz.x *= 0.2;
                    xyz.y *= 0.3;
                    xyz.z *= 0.2;

                    textureIndex = 1;
                    color = 1;
                }

                //Setup transformation matrices
                final switch (i) {
                    case 0: pigLegTransform[i] = translate(-0.3,-0.35,+0.4); break;
                    case 1: pigLegTransform[i] = translate(+0.3,-0.35,+0.4); break;
                    case 2: pigLegTransform[i] = translate(-0.3,-0.35,-0.4); break;
                    case 3: pigLegTransform[i] = translate(+0.3,-0.35,-0.4); break;
                }
            }

            _pig = MultiModel!()([pigBody, pigHead, pigLegs[0], pigLegs[1], pigLegs[2], pigLegs[3]], [cubeIndex, cubeIndex, cubeIndex, cubeIndex, cubeIndex, cubeIndex]);
            _pig.transforms[2..$] = pigLegTransform[];
        }
}

class Player : Living {
    public:
        dvec3 spawn;
        dvec2 camOrient = 0;

        void keyboardMove(bool w, bool a, bool s, bool d, bool jmp) {
            //Create a direction vector using the given keys and camera orientation
            auto move = yaw!dmat3(camOrient.yaw) * dvec3(d*-1 + a*1, 0, w*1 + s*-1).unit;
            if (move[].sum.isNaN) {
                move = 0;
            } else {
                move   *= moveSpeed;
                move.z *= -1;
            }
            _velo = dvec3(move.x, _velo.y, move.z);

            //Jump if grounded
            if (jmp && _grounded) {
                _velo.y = moveSpeed/2;
                _grounded = false;
            }
        }

        void respawn() {
            hp  = 20;
            pos = spawn;
        }

        override void update(double delta, ref dvec3 correction) {
            super.update(delta, correction);
        }

        this()(auto ref dvec3 init) {
            pos   = init;
            spawn = pos;
            _size = dvec3(0.9,1.9,0.9);
            hp    = 20;
        }
}
