#version 450

layout(location = 0) in vec3  inPosition;
layout(location = 1) in uint  inTexture;
layout(location = 2) in vec3  inCoord;
layout(location = 3) in float skyLight;
layout(location = 4) in vec3  inColor;

layout(binding = 0 ) uniform transformMat {
    mat4 transform;
    vec3 fogColor;
    double time;
    double pain;
};

layout(location = 0) out uint textureID;
layout(location = 1) out vec3 fragCoord;
layout(location = 2) out vec3 outColor;

void main() {
    gl_Position = transform * vec4(inPosition.xyz, 1.0);
    gl_Position.z *= gl_Position.z;

    //Pass on needed attributes to fragment shader
    textureID = inTexture;
    fragCoord = inCoord;
    outColor  = inColor;
    
    //Apply sky lighting based on time
    float sky = float(1 - abs(time - 0.5)*2) * skyLight;
    if (outColor.x < sky) outColor.x = sky;
    if (outColor.y < sky) outColor.y = sky;
    if (outColor.z < sky) outColor.z = sky;
}

