#version 450

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outColor;

layout(binding = 0 ) uniform transformMat {
    mat4 transform;
};

void main() {
    gl_Position = transform * vec4(inPosition.xyz, 1.0);
    gl_Position.z = gl_Position.w;
    outColor = inColor;
}
