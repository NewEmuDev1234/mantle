#version 450

layout(location = 0) flat in uint textureID;
layout(location = 1) in vec3 fragCoord;
layout(location = 2) in vec3 color;

layout(binding = 0 ) uniform transformMat {
    mat4 transform;
    vec3 fogColor;
    double time;
    double pain;
};
layout(set = 1, binding = 1) uniform samplerCube[8] image;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = texture(image[textureID], fragCoord);
    if (outColor.a == 0.0) {
        discard;
    } else {
        //Apply per-vertex lighting and fog effect
        outColor.rgb *= color;
        float fog = gl_FragCoord.z*gl_FragCoord.z;
        outColor.rgb = outColor.rgb*(1-fog) + fogColor*fog;
    }
}
