# Mantle

Minecraft clone with multicolored lighting and infinite world generation (which applies in all directions including up and down). I am no longer developing this as I have lost interest and aren't sure how to implement world saving effectively, feel free to do whatever you want with it.

Dependencies:
- BindBC-SDL, SDL2
- Erupted
- Graphics drivers supporting Vulkan

RAM usage is going to be very high because there is no world saving system, and as a result no chunk unloading either

![](/uploads/88c559b471fb0627d2ed17cc8990b2f2/Screenshot_from_2021-05-09_20-39-54.png)
![](/uploads/9e2f05d8a5a27a46995f92592eda8031/Screenshot_from_2021-05-09_20-35-39.png)
![](/uploads/42dd44c27d05e3ed326db2156038124d/Screenshot_from_2021-05-09_20-47-19.png)
